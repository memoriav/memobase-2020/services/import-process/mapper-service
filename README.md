## Mapper Service 

[Confluence Doku](https://memobase.atlassian.net/wiki/spaces/TBAS/pages/29295472/Service+Mapper)

The mapper service is used to map any source data to the Memobase Datamodel. The service uses a mapping configuration
which can be used to map any source property to a Datamodel object and property.

The mapper can add prefixes to the content and create constant fields in addition to just copying over the 
source value.

The configuration is added via Kafka topic as configured within the service. 


## Submodule

The project contains [import-process](import-process/) as a submodule. This submodule is used for the validation tests.
This ensures that all changes to the mapper service are always compatible with all current mappings.

Managing this submodule can be a bit of a pain.

Whenever there are changes in import-process make sure to go into the import-process folder and execute `git pull` there.
Do not make any changes in the folder itself.

Then run the tests to see if all the tests go through. If they do, then in the main repo commit the import-process folder
so that the new revision is used when loading the submodule!