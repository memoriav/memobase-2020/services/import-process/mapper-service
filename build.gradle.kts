plugins {
    application
    distribution
    jacoco
    // https://plugins.gradle.org/plugin/org.jetbrains.kotlin.jvm
    id("org.jetbrains.kotlin.jvm") version "2.0.20"
    // https://plugins.gradle.org/plugin/io.ktor.plugin
    id("io.ktor.plugin") version "2.3.12"
    // https://plugins.gradle.org/plugin/io.freefair.git-version
    id("io.freefair.git-version") version "8.10"
    // https://plugins.gradle.org/plugin/org.jetbrains.kotlin.plugin.serialization
    id("org.jetbrains.kotlin.plugin.serialization") version "2.0.20"
    // id("org.jlleitschuh.gradle.ktlint") version "12.1.1"
    // https://plugins.gradle.org/plugin/org.jetbrains.dokka
    id("org.jetbrains.dokka") version "1.9.20"
}

group = "ch.memobase"

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

application {
    mainClass.set("ch.memobase.App")
    tasks.withType<Tar>().configureEach {
        archiveFileName = "app.tar"
    }
}

repositories {
    mavenCentral()
    maven {
        setUrl("https://gitlab.switch.ch/api/v4/projects/1324/packages/maven")
    }
}

dependencies {

    implementation("io.ktor:ktor-server-core-jvm")
    implementation("io.ktor:ktor-server-netty-jvm")

    // https://gitlab.switch.ch/memoriav/memobase-2020/libraries/memobase-kafka-utils/-/tags
    implementation("ch.memobase:memobase-kafka-utils:0.3.7")
    // https://gitlab.switch.ch/memoriav/memobase-2020/libraries/service-utilities/-/tags
    implementation("ch.memobase:memobase-service-utilities:4.15.3")

    // https://central.sonatype.com/artifact/org.jetbrains.kotlinx/kotlinx-serialization-json
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.7.1")

    // https://central.sonatype.com/artifact/org.jetbrains.kotlin/kotlin-reflect
    implementation("org.jetbrains.kotlin:kotlin-reflect:2.0.20")

    // Logging Imports
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-api
    implementation("org.apache.logging.log4j:log4j-api:2.23.1")
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-core
    implementation("org.apache.logging.log4j:log4j-core:2.23.1")
    // https://central.sonatype.com/artifact/org.apache.logging.log4j/log4j-slf4j2-impl
    implementation("org.apache.logging.log4j:log4j-slf4j2-impl:2.23.1")

    // Kafka Imports
    // https://central.sonatype.com/artifact/org.apache.kafka/kafka-streams
    implementation("org.apache.kafka:kafka-streams:3.8.0")


    // https://central.sonatype.com/artifact/org.apache.jena/apache-jena
    implementation("org.apache.jena:apache-jena:5.1.0")
    // https://central.sonatype.com/artifact/org.apache.jena/jena-shacl
    implementation("org.apache.jena:jena-shacl:5.1.0")
    // JSON Parser
    // https://central.sonatype.com/artifact/com.beust/klaxon
    implementation("com.beust:klaxon:5.6")

    // YAML Parser
    // https://central.sonatype.com/artifact/com.charleskorn.kaml/kaml?smo=true
    implementation("com.charleskorn.kaml:kaml:0.61.0")
    // https://central.sonatype.com/artifact/org.snakeyaml/snakeyaml-engine
    implementation("org.snakeyaml:snakeyaml-engine:2.7")


    // HTTP Client
    testImplementation("io.ktor:ktor-client")
    testImplementation("io.ktor:ktor-client-cio")

    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter
    testImplementation("org.junit.jupiter:junit-jupiter:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-engine
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-api
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.11.0")
    // https://central.sonatype.com/artifact/org.junit.jupiter/junit-jupiter-params
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.11.0")
    // https://central.sonatype.com/artifact/org.assertj/assertj-core
    testImplementation("org.assertj:assertj-core:3.26.0")
    // https://central.sonatype.com/artifact/org.apache.kafka/kafka-streams-test-utils
    testImplementation("org.apache.kafka:kafka-streams-test-utils:3.8.0")
}

configurations {
    all {
        exclude(group = "org.slf4j", module = "slf4j-log4j12")
    }
}

tasks.named<Test>("test") {
    useJUnitPlatform()

    testLogging {
        setEvents(mutableListOf("passed", "skipped", "failed"))
    }
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)
    reports {
        csv.required = true
    }
}
