/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.helpers

/**
 * These keys define the values of the mapping file keys. Any changes in here require an update of the
 * related documentation.
 */
object YamlKeys {
    const val URI = "uri"
    const val TYPE = "type"

    // Top level keys defined for the mapping structure of each object type.
    const val RECORD = "record"
    const val PHYSICAL = "physical"
    const val DIGITAL = "digital"
    const val THUMBNAIL = "thumbnail"

    // Field annotation keys
    /**
     * This field is used to denote a place that is denoted by an object or a list of objects and not just as fields on
     * the top level.
     */
    const val PARENT_FIELD = "parentField"

    const val FIELD = "field"

    /**
     * A constant to be inserted for this value.
     */
    const val CONST = "const"

    /**
     * A mapped field with a prefix. The prefix is added to the beginning of the result.
     */
    const val PREFIX = "prefix"
    const val DE = "de"
    const val FR = "fr"
    const val IT = "it"

    val validFieldMapKeys = listOf(
        CONST, PREFIX, FIELD, PARENT_FIELD
    )

    val validMultiLangFieldMapKeys = listOf(
        CONST, PREFIX, FIELD, DE, FR, IT, PARENT_FIELD
    )

    // Literal Mappings on Record (mostly)

    const val NAME = "name"
    const val DESCRIPTIVE_NOTE = "descriptiveNote"
    const val SCOPE_AND_CONTENT = "scopeAndContent"
    const val ABSTRACT = "abstract"
    const val SOURCE = "source"
    const val IS_SPONSORED_BY_MEMORIAV = "isSponsoredByMemoriav"
    const val SAME_AS = "sameAs"
    const val RELATION = "relation"
    const val CONDITIONS_OF_USE = "conditionsOfUse"
    const val CONDITIONS_OF_ACCESS = "conditionsOfAccess"
    const val PROXY_TYPE = "proxy"
    const val DEFAULT_PROXY_TYPE = "proxydirect"
    const val COORDINATES = "coordinates"

    // Rules
    const val RIGHTS = "rights"

    // Places
    const val PLACE_OF_CAPTURE = "placeOfCapture"
    const val RELATED_PLACES = "relatedPlaces"



    // Agent Fields
    const val CREATORS = "creators"
    const val CONTRIBUTORS = "contributors"
    const val RELATION_NAME = "relationName"
    const val HAS_VARIANT_NAME_OF_AGENT = "hasVariantNameOfAgent"
    const val HISTORY = "history"
    const val GENDER = "gender"
    const val HAS_PROFESSION_OR_OCCUPATION = "hasProfessionOrOccupation"
    const val IS_MEMBER_OF = "isMemberOf"
    const val HAS_BIRTH_DATE = "hasBirthDate"
    const val HAS_DEATH_DATE = "hasDeathDate"
    const val HAS_PERIOD_OF_ACTIVITY = "hasPeriodOfActivity"

    const val PRODUCERS = "producers"
    const val RELATED_AGENTS = "relatedAgents"
    const val PUBLISHED_BY = "publishedBy"

    // Agent Types
    const val AGENT = "agent"
    const val CORPORATE_BODY = "corporateBody"
    const val PERSON = "person"

    // skos:Concept
    const val SUBJECT = "subject"
    const val GENRE = "genre"
    const val PREF_LABEL = "prefLabel"

    // Dates
    const val CREATION_DATE = "creationDate"
    const val ISSUED_DATE = "issuedDate"
    const val TEMPORAL = "temporal"

    // rico:Title
    const val TITLES = "titles"

    // rico:Identifier
    const val IDENTIFIERS = "identifiers"

    // rico:Language
    const val LANGUAGES = "languages"

    // Physical Object Keys
    const val PHYSICAL_CHARACTERISTICS = "physicalCharacteristics"
    const val COLOUR = "colour"
    const val DURATION = "duration"

    // Carrier Type
    const val CARRIER_TYPE = "carrierType"

    // Digital Object Keys
    const val LOCATOR = "locator"
}
