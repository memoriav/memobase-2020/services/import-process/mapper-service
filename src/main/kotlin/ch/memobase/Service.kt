/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.kafka.streams.KafkaStreams
import org.apache.logging.log4j.LogManager
import kotlin.system.exitProcess

object Service {
    private val log = LogManager.getLogger(this::class.java)
    private val settings = App.defineSettings("app.yml")
    private val topology = KafkaTopology(settings).prepare().build()
    private val stream = KafkaStreams(topology, settings.kafkaStreamsSettings)

    fun run() {
        stream.use {
            it.start()
            while (stream.state().isRunningOrRebalancing) {
                Thread.sleep(10_000L)
            }
            it.cleanUp()
        }
        log.error("The streams application ended.")
        exitProcess(1)
    }
}


