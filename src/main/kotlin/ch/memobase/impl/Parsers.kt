/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.impl

import ch.memobase.schema.mapping.RecordMap
import com.charleskorn.kaml.EmptyYamlDocumentException
import com.charleskorn.kaml.MissingRequiredPropertyException
import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.decodeFromStream
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import org.apache.logging.log4j.LogManager

object Parsers {
    private val log = LogManager.getLogger(this.javaClass.name)
    /**
     * Parses the config
     */
    fun parseConfig(data: ByteArray): ByteArray {
        return data
    }

    fun parseConfigInPipeline(data: ByteArray): Pair<RecordMap?, String> {
        return try {
            val string = data.decodeToString()
            log.debug(string)
            Pair(Yaml.default.decodeFromStream(data.inputStream()), "success")
        } catch (ex: EmptyYamlDocumentException) {
            Pair(null, "EmptyYamlDocumentException: $ex")
        } catch (ex: MissingRequiredPropertyException) {
            Pair(null, "MissingRequiredPropertyException: $ex}")
        } catch (ex: SerializationException) {
            Pair(null, "SerializationException: $ex")
        } catch (ex: Exception) {
            Pair(null, ex.stackTraceToString())
        }
    }

    fun parseInput(data: String): JsonObject {
        return Json.decodeFromString(data)
    }
}