/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.impl

import ch.memobase.rdf.*
import ch.memobase.rdf.validation.ShapeModel
import ch.memobase.schema.mapping.ProxyValue
import ch.memobase.schema.mapping.RecordType
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.RDFFormat

object Validate {

    private val recordShapeModel = ShapeModel("RecordValidation", RDFFormat.TURTLE_PRETTY)
    private val physicalInstantiationShapeModel = ShapeModel("PhysicalInstantiation", RDFFormat.TURTLE_PRETTY)
    private val digitalInstantiationShapeModel = ShapeModel("DigitalInstantiation", RDFFormat.TURTLE_PRETTY)
    private val thumbnailShapeModel = ShapeModel("ThumbnailValidation", RDFFormat.TURTLE_PRETTY)

    init {
        recordShape()

        agentShape()
        personShape()
        corporateBodyShape()
        dateSetShape()
        placeShape()
        languageShape()
        titleShape()
        identifierShape(recordShapeModel, listOf(RICO.Types.Identifier.main, RICO.Types.Identifier.original, RICO.Types.Identifier.oldMemobase))
        creationRelationShape()
        ruleShape(recordShapeModel, listOf(RICO.Types.Rule.holder))
        skosConceptShape()

        physicalObjectShape()

        identifierShape(physicalInstantiationShapeModel, listOf(RICO.Types.Identifier.main, RICO.Types.Identifier.callNumber))
        carrierTypeShape()
        ruleShape(physicalInstantiationShapeModel, listOf(RICO.Types.Rule.access, RICO.Types.Rule.usage))

        digitalObjectShape()

        identifierShape(digitalInstantiationShapeModel, listOf(RICO.Types.Identifier.main))
        ruleShape(digitalInstantiationShapeModel, listOf(RICO.Types.Rule.access, RICO.Types.Rule.usage))

        thumbnailShape()
        identifierShape(thumbnailShapeModel, listOf(RICO.Types.Identifier.main))
    }

    fun validateRecordModel(model: Model): String {
        val result = recordShapeModel.validate(model)
        return if (result.first) {
            ""
        } else {
            result.second
        }
    }

    fun validateThumbnailShape(model: Model): String {
        val result = thumbnailShapeModel.validate(model)
        return if (result.first) {
            ""
        } else {
            result.second
        }
    }

    fun validatePhysicalShape(model: Model): String {
        val result = physicalInstantiationShapeModel.validate(model)
        return if (result.first) {
            ""
        } else {
            result.second
        }
    }

    fun validateDigitalShape(model: Model): String {
        val result = digitalInstantiationShapeModel.validate(model)
        return if (result.first) {
            ""
        } else {
            result.second
        }
    }

    private fun recordShape() {
        val focusNode = recordShapeModel.addFocusNode("Record")
            .setTargetClass(RICO.Record)
            .isIRINodeKind()

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern(RecordType.entries.joinToString("|") { it.name })

        focusNode
            .addProperty(RICO.isOrWasPartOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.hasOrHadHolder)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RDA.hasSponsoringAgentOfResource)
            .isIRINodeKind()
            .isIn(MB.memoriavInstitution)
            .maxCountCardinality(1)
            .minCountCardinality(0)

        focusNode
            .addProperty(MB.isPublished)
            .isLiteralNodeKind(XSD.boolean)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()
            // + oldMemobaseId
            .maxCountCardinality(3)
            // main & origin
            .minCountCardinality(2)

        focusNode
            .addProperty(RICO.title)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.hasOrHadTitle)
            .isBlankNodeKind()
            .minCountCardinality(1)

        focusNode
            .addProperty(DC.abstract)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(DC.relation)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.conditionsOfUse)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.conditionsOfAccess)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.scopeAndContent)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.source)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(EBUCORE.hasGenre)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.hasOrHadSubject)
            .isBlankNodeKind()

        focusNode
            .addProperty(DC.created)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        focusNode
            .addProperty(DC.issued)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        focusNode
            .addProperty(DC.temporal)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.isOrWasRegulatedBy)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        focusNode
            .addProperty(RICO.hasOrHadLanguage)
            .isBlankNodeKind()

        focusNode
            .addProperty(RDA.hasPlaceOfCapture)
            .isBlankNodeKind()

        focusNode
            .addProperty(DC.spatial)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.recordResourceOrInstantiationIsSourceOfCreationRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(RDA.hasProducer)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.hasPublisher)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.hasInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(3)
            .minCountCardinality(0)
    }

    private fun physicalObjectShape() {
        val physicalObject = physicalInstantiationShapeModel.addFocusNode("PhysicalObject")
            .setTargetClass(RICO.Instantiation)
            .isIRINodeKind()

        physicalObject
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern(RICO.Types.Instantiation.physicalObject)

        physicalObject
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()
            .maxCountCardinality(2)
            .minCountCardinality(1)

        physicalObject
            .addProperty(RICO.physicalCharacteristics)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        physicalObject
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        physicalObject
            .addProperty(RICO.conditionsOfUse)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        physicalObject
            .addProperty(RICO.conditionsOfAccess)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        physicalObject
            .addProperty(RICO.hasCarrierType)
            .isBlankNodeKind()

        physicalObject
            .addProperty(EBUCORE.duration)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .pattern("\\d{2}:\\d{2}(:\\d{2})?")

        physicalObject
            .addProperty(RDA.hasColourContent)
            .isLiteralNodeKind(XSD.string)

        physicalObject
            .addProperty(RICO.isOrWasRegulatedBy)
            .isBlankNodeKind()
            .maxCountCardinality(2)
            .minCountCardinality(1)

        physicalObject
            .addProperty(RICO.isInstantiationOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        physicalObject
            .addProperty(RICO.hasDerivedInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)
    }

    private fun digitalObjectShape() {
        val digitalObject = digitalInstantiationShapeModel.addFocusNode("DigitalObject")
            .setTargetClass(RICO.Instantiation)
            .isIRINodeKind()

        digitalObject
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .minCountCardinality(1)
            .maxCountCardinality(1)
            .pattern(RICO.Types.Instantiation.digitalObject)

        digitalObject
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        digitalObject
            .addProperty(EBUCORE.locator)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(0)

        digitalObject
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        digitalObject
            .addProperty(RICO.conditionsOfUse)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        digitalObject
            .addProperty(RICO.conditionsOfAccess)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        digitalObject
            .addProperty(RICO.isOrWasRegulatedBy)
            .isBlankNodeKind()
            .maxCountCardinality(3)
            .minCountCardinality(2)

        digitalObject
            .addProperty(RICO.isDerivedFromInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        digitalObject
            .addProperty(RICO.hasDerivedInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(0)

        digitalObject
            .addProperty(RICO.isInstantiationOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        digitalObject
            .addProperty(MB.proxyType)
            .pattern(ProxyValue.entries.joinToString("|") { it.name })
            .maxCountCardinality(1)
            .minCountCardinality(1)

        digitalObject
            .addProperty(EBUCORE.duration)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
    }

    private fun thumbnailShape() {
        val thumbnail = thumbnailShapeModel.addFocusNode("Thumbnail")
            .setTargetClass(RICO.Instantiation)
            .isIRINodeKind()

        thumbnail
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern(RICO.Types.Instantiation.thumbnail)

        thumbnail
            .addProperty(RICO.hasOrHadIdentifier)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        thumbnail
            .addProperty(EBUCORE.locator)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        thumbnail
            .addProperty(RICO.isInstantiationOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        thumbnail
            .addProperty(RICO.isDerivedFromInstantiation)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)
    }


    private fun carrierTypeShape() {
        val originCarrierTypeNode = physicalInstantiationShapeModel.addFocusNode("CarrierType")
            .setTargetClass(RICO.CarrierType)
            .isBlankNodeKind()

        originCarrierTypeNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)

        originCarrierTypeNode
            .addProperty(RICO.isCarrierTypeOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)
    }

    private fun agentShape() {
        val focusNode = recordShapeModel.addFocusNode("Agent")
            .setTargetClass(RICO.Agent)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RDA.hasVariantNameOfAgent)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.history)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RDA.hasPeriodOfActivityOfAgent)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.agentIsTargetOfCreationRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(RDA.isProducerOf)
            .isIRINodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RICO.isOrWasSubjectOf)
            .isIRINodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RICO.isPublisherOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
    }

    private fun personShape() {
        val focusNode = recordShapeModel
            .addFocusNode("Person")
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RDA.hasVariantNameOfAgent)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.history)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RDA.hasPeriodOfActivityOfAgent)
            .isBlankNodeKind()

        focusNode
            .addProperty(FOAF.gender)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)

        focusNode
            .addProperty(RDA.hasProfessionOrOccupation)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RDA.isMemberOf)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.hasBirthDate)
            .isBlankNodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RICO.hasDeathDate)
            .isBlankNodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RICO.agentIsTargetOfCreationRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.isOrWasSubjectOf)
            .isIRINodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RICO.isPublisherOf)
            .isIRINodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RDA.isProducerOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
    }

    fun corporateBodyShape() {
        val focusNode = recordShapeModel.addFocusNode("CorporateBody")
            .setTargetClass(RICO.CorporateBody)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)


        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RDA.hasVariantNameOfAgent)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.history)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RICO.descriptiveNote)
            .isLiteralNodeKind(XSD.string, RDF.langString)

        focusNode
            .addProperty(RDA.hasPeriodOfActivityOfAgent)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.agentIsTargetOfCreationRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.isOrWasSubjectOf)
            .isIRINodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RICO.isPublisherOf)
            .isIRINodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RDA.isProducerOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
    }

    private fun dateSetShape() {
        val focusNode = recordShapeModel.addFocusNode("DateSet")
            .setTargetClass(RICO.SingleDate)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.expressedDate)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.isDeathDateOf)
            .isBlankNodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(RICO.isBirthDateOf)
            .isBlankNodeKind()
            .maxCountCardinality(1)
    }

    private fun placeShape() {
        val focusNode = recordShapeModel
            .addFocusNode("Place")
            .isBlankNodeKind()
            .setTargetClass(RICO.Place)

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)

        focusNode
            .addProperty(RDA.isPlaceOfCaptureOf)
            .isIRINodeKind()
            .maxCountCardinality(1)

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(WD.coordinateLocation)
            .isLiteralNodeKind(XSD.string)
    }

    private fun languageShape() {
        val focusNode = recordShapeModel.addFocusNode("Language")
            .setTargetClass(RICO.Language)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("${RICO.Types.Language.content}|${RICO.Types.Language.caption}")

        focusNode
            .addProperty(RICO.isOrWasLanguageOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

    }

    private fun titleShape() {
        val focusNode = recordShapeModel.addFocusNode("Title")
            .setTargetClass(RICO.Title)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.title)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.isOrWasTitleOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("${RICO.Types.Title.main}|${RICO.Types.Title.series}|${RICO.Types.Title.broadcast}")
    }


    private fun identifierShape(model: ShapeModel, typePattern: List<String>) {
        val focusNode = model.addFocusNode("Identifier")
            .setTargetClass(RICO.Identifier)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.identifier)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.isOrWasIdentifierOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern(typePattern.joinToString("|"))
    }

    private fun creationRelationShape() {
        val focusNode = recordShapeModel.addFocusNode("CreationRelation")
            .setTargetClass(RICO.CreationRelation)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(0)

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern("${RICO.Types.CreationRelation.creator}|${RICO.Types.CreationRelation.contributor}")

        focusNode
            .addProperty(RICO.creationRelationHasSource)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.creationRelationHasTarget)
            .isBlankNodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)
    }

    private fun ruleShape(model: ShapeModel, ricoTypePatternValues: List<String>) {
        val focusNode = model.addFocusNode("Rule")
            .setTargetClass(RICO.Rule)
            .isBlankNodeKind()

        focusNode
            .addProperty(RICO.type)
            .isLiteralNodeKind(XSD.string)
            .maxCountCardinality(1)
            .minCountCardinality(1)
            .pattern(ricoTypePatternValues.joinToString("|"))

        focusNode
            .addProperty(RICO.name)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)

        focusNode
            .addProperty(SCHEMA.sameAs)
            .isLiteralNodeKind(XSD.string)

        focusNode
            .addProperty(RICO.regulatesOrRegulated)
            .isIRINodeKind()
            .maxCountCardinality(1)
            .minCountCardinality(1)
    }
    
    private fun skosConceptShape() {
        val focusNode = recordShapeModel.addFocusNode("SkosConcept")
            .setTargetClass(SKOS.Concept)
            .isBlankNodeKind()

        focusNode
            .addProperty(SKOS.prefLabel)
            .isLiteralNodeKind(XSD.string, RDF.langString)
            .maxCountCardinality(3)
            .minCountCardinality(1)

        focusNode
            .addProperty(RICO.isOrWasSubjectOf)
            .isIRINodeKind()
            .maxCountCardinality(1)
    }

    fun printShapeModel(path: String, fileName: String) {
        recordShapeModel.writeModelToFile(path, "$fileName-record")
        physicalInstantiationShapeModel.writeModelToFile(path, "$fileName-physical")
        digitalInstantiationShapeModel.writeModelToFile(path, "$fileName-digital")
        thumbnailShapeModel.writeModelToFile(path, "$fileName-thumbnail")
    }
}
