/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.impl

import ch.memobase.builder.*
import ch.memobase.builder.Record
import ch.memobase.exceptions.InvalidInputException
import ch.memobase.helpers.Reports
import ch.memobase.helpers.YamlKeys.DE
import ch.memobase.helpers.YamlKeys.FR
import ch.memobase.helpers.YamlKeys.IT
import ch.memobase.rdf.*
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.schema.AgentMetadata
import ch.memobase.schema.PlaceMetadata
import ch.memobase.schema.ResourceBuilderOutput
import ch.memobase.schema.ResourceBuilderRenderedOutput
import ch.memobase.schema.mapping.*
import kotlinx.serialization.json.*
import org.apache.jena.rdf.model.Literal
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.apache.kafka.streams.KeyValue
import java.io.StringWriter

class ResourceBuilder private constructor(
    private val source: JsonObject,
    private val config: RecordMap,
    private val reports: Reports,
    private val originalId: String,
    private val institutionId: String,
    private val recordSetId: String,
    private val isPublished: Boolean,
    private val allFields: MutableList<String>,
) {
    private val warnings: MutableList<String> = mutableListOf()
    private val notes: MutableList<String> = mutableListOf()

    companion object {
        fun initializeBuilder(
            currentKey: String,
            source: JsonObject,
            config: RecordMap,
            reports: Reports,
            institutionId: String,
            recordSetId: String,
            isPublished: Boolean
        ): Pair<ResourceBuilder?, Report> {
            return when (val recordIdValue = source[config.record.uri]) {
                is JsonPrimitive -> {
                    if (recordIdValue.isString) {
                        if (recordIdValue.content.isEmpty()) {
                            Pair(
                                null,
                                reports.fatal(
                                    currentKey,
                                    "Memobase id provided in field '${config.record.uri}' is empty."
                                )
                            )
                        } else {
                            Pair(
                                ResourceBuilder(
                                    source = source,
                                    config = config,
                                    reports = reports,
                                    originalId = recordIdValue.content,
                                    institutionId = institutionId,
                                    recordSetId = recordSetId,
                                    isPublished = isPublished,
                                    allFields = mutableListOf(config.record.uri)
                                ), reports.success(recordIdValue.content, recordIdValue.content)
                            )
                        }
                    } else {
                        Pair(
                            null,
                            reports.fatal(currentKey, "Cannot use an identifier or boolean as the uri.")
                        )
                    }
                }

                is JsonObject -> {
                    Pair(
                        null, reports.fatal(
                            currentKey,
                            "The field mapped to uri '${config.record.uri}' is an object. This is not allowed."
                        )
                    )
                }

                is JsonArray -> {
                    Pair(
                        null, reports.fatal(
                            currentKey,
                            "The field mapped to uri '${config.record.uri}' is a list. This is not allowed."
                        )
                    )
                }

                null -> {
                    Pair(
                        null, reports.fatal(
                            currentKey,
                            "Source data does not contain the field '${config.record.uri}'. This is required to generate the memobase id."
                        )
                    )
                }
            }
        }
    }


    private fun extractRecordTypeValue(id: String): Report {
        val recordType = config.record.type
        recordType.const?.let {
            return reports.success(id, it.name)
        }

        recordType.map?.let { mapFieldName ->
            if (source.containsKey(mapFieldName)) {
                return when (val recordTypeValue = source[mapFieldName]) {
                    is JsonArray -> reports.fatal(
                        id,
                        "The mapped field '$mapFieldName' for record.type is a list in the source. This is invalid."
                    )

                    is JsonObject -> reports.fatal(
                        id,
                        "The mapped field '$mapFieldName' for record.type is an object in the source. This is invalid."
                    )

                    is JsonPrimitive -> {
                        val value = recordTypeValue.content
                        allFields.add(mapFieldName)
                        return reports.success(id, value)
                    }

                    JsonNull -> reports.fatal(
                        id,
                        "The mapped field '$mapFieldName' for record.type is null in the source."
                    )

                    null -> reports.fatal(
                        id,
                        "The mapped field '$mapFieldName' for record.type is null in the source."
                    )
                }
            }
        }

        return reports.fatal(id, "No record type extracted as no mapping was defined for 'type'.")
    }

    fun generateRecord(): KeyValue<String, ResourceBuilderOutput> {
        val recordType = extractRecordTypeValue(originalId)
        if (recordType.status == ReportStatus.fatal) {
            return KeyValue(originalId, out(null, emptyList(), recordType))
        }

        val record = Record(
            originalId,
            recordType.message,
            recordSetId,
            institutionId,
            config.record.isSponsoredByMemoriav,
            isPublished
        )

        config.record.identifiers.let { identifiers ->
            val report = mapOriginalIdentifier(originalId, identifiers.original, record)
            if (report.status == ReportStatus.fatal) {
                return KeyValue(record.uri, out(record, emptyList(), report))
            }
            identifiers.oldMemobase?.let { oldMemobaseId ->
                extractFieldValue(oldMemobaseId, record).forEach { item ->
                    record.addOldMemobaseIdentifier(item)
                }
            }
        }

        config.record.titles.let { titles ->
            for (title in titles) {
                title.main?.map {
                    record.addMainTitle(extractMultiLangField(it, record).flatten())
                }

                title.series?.map {
                    record.addSeriesTitle(extractMultiLangField(it, record).flatten())
                }

                title.broadcast?.map {
                    record.addBroadcastTitle(extractMultiLangField(it, record).flatten())
                }
            }
        }

        config.record.abstract?.map {
            record.addLiteral(DC.abstract, extractMultiLangField(it, record).flatten())
        }

        config.record.relation?.map {
            record.addLiteral(DC.relation, extractMultiLangField(it, record).flatten())
        }

        config.record.conditionsOfUse?.forEach {
            record.addLiteral(RICO.conditionsOfUse, extractMultiLangField(it, record).flatten())
        }

        config.record.conditionsOfAccess?.forEach {
            record.addLiteral(RICO.conditionsOfAccess, extractMultiLangField(it, record).flatten())
        }

        config.record.descriptiveNote?.map {
            record.addLiteral(RICO.descriptiveNote, extractMultiLangField(it, record).flatten())
        }

        config.record.sameAs?.map {
            record.addLiteral(SCHEMA.sameAs, extractFieldValue(it, record))
        }

        config.record.scopeAndContent?.map {
            record.addLiteral(RICO.scopeAndContent, extractMultiLangField(it, record).flatten())
        }

        config.record.source?.map {
            record.addLiteral(RICO.source, extractMultiLangField(it, record).flatten())
        }

        config.record.genre?.map {
            extractMultiLangField(it.prefLabel, record)
                .map { literals -> record.addGenre(literals) }
        }

        config.record.subject?.map {
            extractMultiLangField(it.prefLabel, record)
                .map { literals ->
                    record.addSubject(literals)
                }
        }

        config.record.creationDate?.let { creationDateField ->
            source[creationDateField]?.let { creationDateValue ->
                extractStringsFromJsonElement(creationDateValue).forEach {
                    record.addDate(DC.created, it)
                }
            }
        }

        config.record.issuedDate?.let { issuedDateField ->
            source[issuedDateField]?.let { issuedDateValue ->
                extractStringsFromJsonElement(issuedDateValue).forEach {
                    record.addDate(DC.issued, it)
                }
            }
        }

        config.record.temporal?.let { temporalField ->
            source[temporalField]?.let { temporalValue ->
                extractStringsFromJsonElement(temporalValue).forEach {
                    record.addDate(DC.temporal, it)
                }
            }
        }

        config.record.rights?.holder.let {
            record.addHolderRule(extractMultiLangField(it, record).flatten())
        }

        config.record.languages?.let { language ->
            language.content?.map {
                extractMultiLangField(it, record).map { literals ->
                    record.addContentLanguage(literals)
                }
            }

            language.caption?.map {
                extractMultiLangField(it, record).map { literals ->
                    record.addCaptionLanguage(literals)
                }
            }
        }

        config.record.placeOfCapture?.map {
            mapPlace(it, record).forEach { placeMetadata ->
                record.addPlaceOfCapture(placeMetadata.names, placeMetadata.sameAs, placeMetadata.coordinates)
            }
        }

        config.record.relatedPlaces?.map {
            mapPlace(it, record).forEach { placeMetadata ->
                record.addRelatedPlaces(placeMetadata.names, placeMetadata.sameAs, placeMetadata.coordinates)
            }
        }

        config.record.creators?.map { agent ->
            agent.person?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addCreator(RICO.Person, agentMetadata)
                }
            }
            agent.corporateBody?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addCreator(RICO.CorporateBody, agentMetadata)
                }
            }
            agent.agent?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addCreator(RICO.Agent, agentMetadata)
                }
            }
        }

        config.record.contributors?.map { agent ->
            agent.person?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addContributor(RICO.Person, agentMetadata)
                }
            }
            agent.corporateBody?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addContributor(RICO.CorporateBody, agentMetadata)
                }
            }
            agent.agent?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addContributor(RICO.Agent, agentMetadata)
                }
            }
        }

        config.record.producers?.map { agent ->
            agent.person?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addProducer(RICO.Person, agentMetadata)
                }
            }
            agent.corporateBody?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addProducer(RICO.CorporateBody, agentMetadata)
                }
            }
            agent.agent?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addProducer(RICO.Agent, agentMetadata)
                }
            }
        }

        config.record.publishedBy?.map { agent ->
            agent.person?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addPublisher(RICO.Person, agentMetadata)
                }
            }
            agent.corporateBody?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addPublisher(RICO.CorporateBody, agentMetadata)
                }
            }
            agent.agent?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addPublisher(RICO.Agent, agentMetadata)
                }
            }
        }

        config.record.relatedAgents?.map { agent ->
            agent.person?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addSubjectAgent(RICO.Person, agentMetadata)
                }
            }
            agent.corporateBody?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addSubjectAgent(RICO.CorporateBody, agentMetadata)
                }
            }
            agent.agent?.let {
                unpackAgents(it, record).forEach { agentMetadata ->
                    record.addSubjectAgent(RICO.Agent, agentMetadata)
                }
            }
        }

        val result = Validate.validateRecordModel(record.model)
        return if (result.isNotEmpty()) {
            KeyValue(record.uri, out(record, emptyList(), reports.fatal(originalId, result)))
        } else {
            KeyValue(record.uri, out(record, emptyList(), reports.success(originalId, "")))
        }
    }

    fun generatePhysicalObject(resources: List<IResource>, report: Report): ResourceBuilderOutput {
        val physical = config.physical ?: return out(null, resources, report)

        val physicalObject = PhysicalObject(
            sourceId = originalId,
            recordSetId = recordSetId,
            institutionId = institutionId,
            count = 1,
        )

        physical.identifiers?.callNumber?.let { field ->
            extractFieldValue(field, physicalObject).forEach {
                physicalObject.addCallNumber(it)
            }
        }

        physical.physicalCharacteristics?.forEach {
            physicalObject.addLiteral(RICO.physicalCharacteristics, extractMultiLangField(it, physicalObject).flatten())
        }

        physical.descriptiveNote?.forEach {
            physicalObject.addLiteral(RICO.descriptiveNote, extractMultiLangField(it, physicalObject).flatten())
        }

        physical.conditionsOfAccess?.forEach {
            physicalObject.addLiteral(RICO.conditionsOfAccess, extractMultiLangField(it, physicalObject).flatten())
        }

        physical.conditionsOfUse?.forEach {
            physicalObject.addLiteral(RICO.conditionsOfUse, extractMultiLangField(it, physicalObject).flatten())
        }

        physical.carrierType?.forEach {
            extractMultiLangField(it, physicalObject).map { literals ->
                physicalObject.addCarrierType(literals)
            }
        }

        physical.duration?.let { durationField ->
            source[durationField]?.let { jsonElement ->
                extractStringsFromJsonElement(jsonElement).forEach {
                    physicalObject.addLiteral(EBUCORE.duration, listOf(physicalObject.literal(it)))
                }
            }
        }

        physical.colour?.forEach {
            physicalObject.addLiteral(RDA.hasColourContent, extractMultiLangField(it, physicalObject).flatten())
        }

        physical.rights.access?.forEach {
            physicalObject.addAccessRule(extractMultiLangField(it, physicalObject).flatten())
        }

        physical.rights.usage?.let {
            val name = extractMultiLangField(it.name, physicalObject)
            val sameAs = extractFieldValue(it.sameAs, physicalObject)
            physicalObject.addUsageRule(name.flatten(), sameAs)
        }

        resources.forEach {
            when (it) {
                is Record -> {
                    it.addInstantiation(physicalObject)
                    physicalObject.addRecord(it)
                }
            }
        }
        val result = Validate.validatePhysicalShape(physicalObject.model)
        return if (result != "") {
            out(null, resources, reports.fatal(report.id, result))
        } else {
            out(physicalObject, resources, report)
        }
    }

    fun generateDigitalObject(resources: List<IResource>, report: Report): ResourceBuilderOutput {
        val digital = config.digital ?: return out(null, resources, report)
        var hasMappedValues = false

        val digitalObject = DigitalObject(
            sourceId = originalId,
            recordSetId = recordSetId,
            institutionId = institutionId,
            count = 1,
            hasProxyType = true,
        )

        digital.proxy.let { proxy ->
            when (proxy) {
                is ProxyConstant -> digitalObject.addLiteral(
                    MB.proxyType,
                    listOf(digitalObject.literal(proxy.value.name))
                )

                is ProxyMap -> source[proxy.name]?.let { jsonElement ->
                    extractStringsFromJsonElement(jsonElement).forEach { value ->
                        if (ProxyValue.entries.map { entry -> entry.name }.contains(value)) {
                            digitalObject.addLiteral(MB.proxyType, listOf(digitalObject.literal(value)))
                        } else {
                            return out(
                                null,
                                resources,
                                reports.fatal(originalId, "Digital object was mapped to invalid value: $value.")
                            )
                        }
                    }
                }
            }
        }

        digital.locator?.let {
            val literals = extractFieldValue(it, digitalObject)
            if (literals.isNotEmpty()) {
                digitalObject.addLiteral(EBUCORE.locator, literals)
                hasMappedValues = true
            }
        }

        digital.duration?.let {
            val jsonElement = source[it]
            if (jsonElement != null)
                extractStringsFromJsonElement(jsonElement).forEach { content ->
                    digitalObject.addLiteral(EBUCORE.duration, digitalObject.literal(content))
                    hasMappedValues = true
                }
        }

        digital.conditionsOfAccess?.forEach {
            val literals = extractMultiLangField(it, digitalObject)
            if (literals.isNotEmpty()) {
                digitalObject.addLiteral(RICO.conditionsOfAccess, literals.flatten())
                hasMappedValues = true
            }
        }

        digital.conditionsOfUse?.forEach {
            val literals = extractMultiLangField(it, digitalObject)
            if (literals.isNotEmpty()) {
                digitalObject.addLiteral(RICO.conditionsOfUse, literals.flatten())
                hasMappedValues = true
            }
        }

        digital.descriptiveNote?.forEach {
            val literals = extractMultiLangField(it, digitalObject)
            if (literals.isNotEmpty()) {
                digitalObject.addLiteral(RICO.descriptiveNote, literals.flatten())
                hasMappedValues = true
            }
        }

        digital.rights.access?.forEach {
            val names = extractMultiLangField(it, digitalObject)
            if (names.isNotEmpty()) {
                digitalObject.addAccessRule(names.flatten())
                hasMappedValues = true
            }
        }

        digital.rights.usage?.let {
            val names = extractMultiLangField(it.name, digitalObject)
            val sameAs = extractFieldValue(it.sameAs, digitalObject)
            if (names.isNotEmpty()) {
                digitalObject.addUsageRule(names.flatten(), sameAs)
                hasMappedValues = true
            }
        }

        if (hasMappedValues) {
            resources.forEach {
                when (it) {
                    is Record -> {
                        it.addInstantiation(digitalObject)
                        digitalObject.addRecord(it)
                    }

                    is PhysicalObject -> {
                        it.addDerivedInstantiation(digitalObject)
                        digitalObject.addIsDerivedFromInstantiation(it)
                    }
                }
            }
            val result = Validate.validateDigitalShape(digitalObject.model)
            return if (result != "") {
                out(null, resources, reports.fatal(report.id, result))
            } else {
                out(digitalObject, resources, report)
            }
        }
        return out(null, resources, report)
    }

    fun generateThumbnailObject(resources: List<IResource>, report: Report): ResourceBuilderOutput {
        val thumbnail = config.thumbnail ?: return out(null, resources, report)
        val locatorField = source[thumbnail.locator] ?: return out(null, resources, report)
        val locatorValues = extractStringsFromJsonElement(locatorField)
        return if (locatorValues.isEmpty())
            out(null, resources, report)
        else if (locatorValues.size > 1)
            out(
                null,
                resources,
                reports.fatal(
                    report.id,
                    "Found ${locatorValues.size} values for the thumbnail.locator in field ${thumbnail.locator}. Only a single value is supported."
                )
            )
        else {
            val thumbnailObject = Thumbnail(
                originalId = originalId,
                recordSetId = recordSetId,
                institutionId = institutionId,
                count = 1
            )
            thumbnailObject.addLiteral(EBUCORE.locator, thumbnailObject.literal(locatorValues.first()))

            resources.forEach {
                when (it) {
                    is Record -> {
                        it.addInstantiation(thumbnailObject)
                        thumbnailObject.addRecord(it)
                    }

                    is DigitalObject -> {
                        it.addDerivedInstantiation(thumbnailObject)
                        thumbnailObject.addIsDerivedFromInstantiation(it)
                    }
                }
            }

            val validated = Validate.validateThumbnailShape(thumbnailObject.model)
            if (validated == "") {
                out(thumbnailObject, resources, report)
            } else {
                out(null, resources, reports.fatal(report.id, validated))
            }
        }
    }

    fun writeRecord(format: RDFFormat, resources: List<IResource>, report: Report): ResourceBuilderRenderedOutput {
        val warnings = if (warnings.isNotEmpty()) {
            "\n\nWarnings:\n" +
                    warnings.joinToString("\n")
        } else {
            ""
        }
        val notes = if (notes.isNotEmpty()) {

            "\n\nNotes:\n" + notes.joinToString("\n")
        } else {
            ""
        }

        return StringWriter().use { writer ->
            for (r in resources) {
                RDFDataMgr.write(writer, r.model, format)
            }
            val output = writer.toString().trim()
            if (report.status == ReportStatus.fatal) {
                ResourceBuilderRenderedOutput(
                    output,
                    reports.fatal(
                        report.id,
                        report.message + notes + warnings
                    )
                )
            } else {
                ResourceBuilderRenderedOutput(output, reports.success(report.id, notes + warnings))
            }
        }
    }

    private fun mapOriginalIdentifier(id: String, originalFieldName: String, record: Record): Report {
        val originalIdJsonElement = source[originalFieldName] ?: return reports.fatal(
            id,
            "For required original id $originalFieldName no field found in source document."
        )
        val originalIdValue = extractStringsFromJsonElement(originalIdJsonElement)
        return if (originalIdValue.isEmpty())
            return reports.fatal(id, "No value found for mapped field $originalFieldName. Original id is required.")
        else if (originalIdValue.size > 1)
            return reports.fatal(
                id,
                "Found multiple values in mapped field $originalFieldName for original id. Only one value is supported."
            )
        else {
            allFields.add(originalFieldName)
            record.addOriginalIdentifier(record.literal(originalIdValue.first()))
            reports.success(id, originalIdValue.first())
        }
    }

    private fun extractMultiLangField(
        multiLangField: MultiLangField?,
        recordResource: RecordResource,
    ): List<List<Literal>> {
        return extractMultiLangField(multiLangField, source, recordResource)
    }

    private fun extractMultiLangField(
        multiLangField: MultiLangField?,
        sourceObject: JsonObject,
        recordResource: RecordResource,
    ): List<List<Literal>> {
        return when (multiLangField) {
            is Field -> extractFieldValue(multiLangField, sourceObject, recordResource).map {
                listOf(it)
            }

            is LanguageField -> {
                val de = extractFieldValue(multiLangField.de, DE, sourceObject, recordResource)
                val fr = extractFieldValue(multiLangField.fr, FR, sourceObject, recordResource)
                val it = extractFieldValue(multiLangField.it, IT, sourceObject, recordResource)

                if (de.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag de: $de.")
                } else if (fr.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag fr: $fr.")
                } else if (it.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag it: $it.")
                } else {
                    listOf(listOfNotNull(de.firstOrNull(), fr.firstOrNull(), it.firstOrNull()))
                }
            }

            is FrenchField -> {
                val fr = extractFieldValue(multiLangField.fr, FR, sourceObject, recordResource)

                if (fr.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag fr: $fr.")
                } else {
                    listOf(listOfNotNull(fr.firstOrNull()))
                }
            }

            is FrenchItalianField -> {
                val fr = extractFieldValue(multiLangField.fr, FR, sourceObject, recordResource)
                val it = extractFieldValue(multiLangField.it, IT, sourceObject, recordResource)

                if (fr.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag fr: $fr.")
                } else if (it.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag it: $it.")
                } else {
                    listOf(listOfNotNull(fr.firstOrNull(), it.firstOrNull()))
                }
            }

            is GermanField -> {
                val de = extractFieldValue(multiLangField.de, DE, sourceObject, recordResource)

                if (de.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag de: $de.")
                } else {
                    listOf(listOfNotNull(de.firstOrNull()))
                }
            }

            is GermanFrenchField -> {
                val fr = extractFieldValue(multiLangField.fr, FR, sourceObject, recordResource)
                val de = extractFieldValue(multiLangField.de, DE, sourceObject, recordResource)

                if (fr.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag fr: $fr.")
                } else if (de.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag de: $de.")
                } else {
                    listOf(listOfNotNull(fr.firstOrNull(), de.firstOrNull()))
                }
            }

            is GermanItalianField -> {
                val it = extractFieldValue(multiLangField.it, IT, sourceObject, recordResource)
                val de = extractFieldValue(multiLangField.de, DE, sourceObject, recordResource)

                if (it.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag it: $it.")
                } else if (de.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag de: $de.")
                } else {
                    listOf(listOfNotNull(it.firstOrNull(), de.firstOrNull()))
                }
            }

            is ItalianField -> {
                val it = extractFieldValue(multiLangField.it, IT, sourceObject, recordResource)

                if (it.size > 1) {
                    throw InvalidInputException("Too many inputs for language tag it: $it.")
                } else {
                    listOf(listOfNotNull(it.firstOrNull()))
                }
            }

            null -> emptyList()
        }
    }

    private fun extractFieldValue(
        field: Field,
        language: String,
        sourceObject: JsonObject,
        recordResource: RecordResource
    ): List<Literal> {
        return extractFieldValue(field, sourceObject, recordResource).map { value ->
            recordResource.langLiteral(value.string, language)
        }
    }


    private fun mapPlace(place: Place, recordResource: RecordResource): List<PlaceMetadata> {
        fun innerMapPlace(
            place: Place,
            childObject: JsonObject
        ): PlaceMetadata? {
            val names = extractMultiLangField(place.name, childObject, recordResource)
            val sameAs = place.sameAs
                ?.mapNotNull { childObject[it] }
                ?.flatMap { extractStringsFromJsonElement(it) }
                ?.map { value -> recordResource.literal(value) }
                ?: emptyList()
            val coordinates = place.coordinates?.let { extractFieldValue(it, childObject, recordResource) }

            if (names.isEmpty()) {
                return null
            }

            return PlaceMetadata(
                names.flatten(), sameAs, coordinates?.firstOrNull()
            )
        }

        return if (place.parentField != null) {
            return when (val childObject = source[place.parentField]) {
                null -> {
                    return emptyList()
                }

                is JsonPrimitive -> {
                    warnings.add("Mapped field '${place.parentField}' should be an object or array, but found a string value instead.")
                    return emptyList()
                }

                is JsonArray -> {
                    childObject.mapNotNull {
                        when (it) {
                            is JsonArray -> null
                            is JsonObject -> innerMapPlace(place, it)
                            is JsonPrimitive -> null
                            JsonNull -> null
                        }
                    }
                }

                is JsonObject -> {
                    listOfNotNull(innerMapPlace(place, childObject))
                }
            }
        } else {
            // if the place is not in an object, only names can be extracted, no sameAs / coordinates.
            extractMultiLangField(place.name, source, recordResource).map { name ->
                PlaceMetadata(name, emptyList(), null)
            }
        }
    }


    private fun unpackAgents(
        agentDataWithRelation: AgentData,
        resource: RecordResource
    ): List<AgentMetadata> {
        fun mapAgent(
            childObject: JsonObject,
        ): AgentMetadata? {
            val names = extractMultiLangField(agentDataWithRelation.name, childObject, resource)
            if (names.isEmpty()) {
                return null
            }
            return AgentMetadata(
                name = names.flatten(),
                relationNames = agentDataWithRelation.relationName?.map {
                    extractMultiLangField(it, childObject, resource)
                }?.flatten() ?: emptyList(),
                sameAs = agentDataWithRelation.sameAs?.flatMap {
                    extractFieldValue(it, childObject, resource)
                }?.map { it.string },
                hasVariantNameOfAgent = agentDataWithRelation.hasVariantNameOfAgent?.flatMap {
                    extractMultiLangField(it, childObject, resource)
                }?.flatten(),
                hasBirthDate = agentDataWithRelation.hasBirthDate,
                hasDeathDate = agentDataWithRelation.hasDeathDate,
                gender = agentDataWithRelation.gender?.let {
                    extractFieldValue(it, childObject, resource).firstOrNull()?.string
                },
                history = agentDataWithRelation.history?.flatMap {
                    extractMultiLangField(it, childObject, resource)
                }?.flatten(),
                hasPeriodOfActivity = agentDataWithRelation.hasPeriodOfActivity?.map {
                    it
                },
                isMemberOf = agentDataWithRelation.isMemberOf?.flatMap {
                    extractMultiLangField(it, childObject, resource)
                }?.flatten(),
                descriptiveNote = agentDataWithRelation.descriptiveNote?.flatMap {
                    extractMultiLangField(it, childObject, resource)
                }?.flatten(),
                hasProfessionOrOccupation = agentDataWithRelation.hasProfessionOrOccupation?.flatMap {
                    extractMultiLangField(it, childObject, resource)
                }?.flatten(),
            )
        }


        return if (agentDataWithRelation.parentField != null) {
            return when (val childObject = source[agentDataWithRelation.parentField]) {
                null -> {
                    emptyList()
                }

                is JsonPrimitive -> {
                    warnings.add("Mapped field '${agentDataWithRelation.parentField}' should be an object or array, but found a string value instead.")
                    emptyList()
                }

                is JsonArray -> {
                    childObject.mapNotNull {
                        when (it) {
                            is JsonArray -> null
                            is JsonObject -> mapAgent(it)
                            is JsonPrimitive -> null
                            JsonNull -> null
                        }
                    }
                }

                is JsonObject -> {
                    listOfNotNull(mapAgent(childObject))
                }

                else -> {
                    warnings.add("Mapped field '${agentDataWithRelation.parentField}' has invalid type: ${agentDataWithRelation::class.java.name}.")
                    emptyList()
                }
            }
        } else {
            listOfNotNull(mapAgent(source))
        }
    }

    private fun extractFieldValue(
        field: Field,
        recordResource: RecordResource,
    ): List<Literal> {
        return extractFieldValue(field, source, recordResource)
    }

    private fun extractFieldValue(
        field: Field,
        sourceObject: JsonObject,
        recordResource: RecordResource,
    ): List<Literal> {
        return when (field) {
            is ConstantField -> listOf(recordResource.literal(field.value))
            is MapField -> {
                return extractFieldFromObject(sourceObject, field.name, recordResource)
            }

            is PrefixField -> {
                return extractPrefixFieldFromObject(
                    sourceObject,
                    field.prefix.field,
                    field.prefix.value,
                    recordResource
                )
            }

            is FieldWithParent -> {
                when (field) {
                    is MapFieldWithParent -> {
                        when (val childObject = sourceObject[field.parentField]) {
                            is JsonArray -> childObject.flatMap { item ->
                                when (item) {
                                    is JsonArray -> throw InvalidInputException("List in lists are not supported: ${field.parentField}.")
                                    is JsonObject -> extractFieldFromObject(item, field.name, recordResource)
                                    is JsonPrimitive -> throw InvalidInputException("Expected an object here and not a string: ${field.parentField}.")
                                    JsonNull -> emptyList()
                                }

                            }

                            is JsonObject -> extractFieldFromObject(childObject, field.name, recordResource)
                            is JsonPrimitive -> throw IllegalArgumentException("Expected an object or array for field ${field.parentField}, but got a string instead.")
                            JsonNull -> emptyList()
                            null -> emptyList()
                        }
                    }

                    is PrefixFieldWithParent -> {
                        when (val childObject = sourceObject[field.parentField]) {
                            is JsonArray -> childObject.flatMap { item ->
                                when (item) {
                                    is JsonArray -> throw InvalidInputException("List in lists are not supported: ${field.parentField}.")
                                    is JsonObject -> extractPrefixFieldFromObject(
                                        item,
                                        field.prefix.field,
                                        field.prefix.value,
                                        recordResource
                                    )

                                    is JsonPrimitive -> throw InvalidInputException("Expected an object here and not a string: ${field.parentField}.")
                                    JsonNull -> emptyList()
                                }

                            }

                            is JsonObject -> extractPrefixFieldFromObject(
                                childObject,
                                field.prefix.field,
                                field.prefix.value,
                                recordResource
                            )

                            is JsonPrimitive -> throw IllegalArgumentException("Expected an object or array for field ${field.parentField}, but got a string instead.")
                            JsonNull -> emptyList()
                            null -> emptyList()
                        }
                    }
                }


            }
        }
    }

    private fun extractPrefixFieldFromObject(
        sourceObject: JsonObject,
        field: String,
        prefix: String,
        recordResource: RecordResource
    ): List<Literal> {
        val value = sourceObject[field]
        if (value != null) {
            val prefixFieldValue = extractStringsFromJsonElement(value)
            val literals = prefixFieldValue.mapNotNull {
                if (it.isNotEmpty())
                    recordResource.literal(prefix + it)
                else
                    null
            }
            if (literals.isNotEmpty())
                allFields.add(field)
            return literals
        } else {
            return emptyList()
        }
    }

    private fun extractFieldFromObject(
        sourceObject: JsonObject,
        fieldName: String,
        recordResource: RecordResource
    ): List<Literal> {
        val value = sourceObject[fieldName]
        if (value != null) {
            val mapFieldValues = extractStringsFromJsonElement(value)
            val literals = mapFieldValues.mapNotNull {
                if (it.isNotEmpty())
                    recordResource.literal(it)
                else
                    null
            }
            if (literals.isNotEmpty()) {
                allFields.add(fieldName)
            }
            return literals
        } else {
            return emptyList()
        }
    }


    private fun extractStringsFromJsonElement(jsonElement: JsonElement): List<String> {
        return when (jsonElement) {
            is JsonArray -> {
                jsonElement.mapNotNull {
                    when (it) {
                        is JsonArray -> throw InvalidInputException("Does not support lists in a list.")
                        is JsonObject -> throw InvalidInputException("Does not support object.")
                        is JsonPrimitive -> it.content
                        JsonNull -> null
                    }
                }
            }

            is JsonObject -> throw InvalidInputException("Does not support object.")
            is JsonPrimitive -> listOf(jsonElement.content)
            JsonNull -> emptyList()
        }
    }

    private fun out(resource: IResource?, resources: List<IResource>, report: Report): ResourceBuilderOutput {
        resource.let {
            if (it != null) {
                return ResourceBuilderOutput(this, resources + it, report)
            } else {
                return ResourceBuilderOutput(this, resources, report)
            }
        }
    }
}
