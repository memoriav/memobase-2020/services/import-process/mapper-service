/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.builder

import ch.memobase.rdf.NS
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import org.apache.jena.rdf.model.*
import org.apache.logging.log4j.LogManager

abstract class RecordResource(institutionId: String) : IResource {
    private val log = LogManager.getLogger(this::class.java.name)

    final override val model: Model = ModelFactory.createDefaultModel()

    abstract val resource: Resource
    protected val institutionUri: Resource = institutionUri(institutionId)

    init {
        model.setNsPrefixes(NS.prefixMapping)
    }

    protected fun addRdfType(type: Resource) {
        resource.addProperty(RDF.type, type)
    }

    protected fun addIdentifier(ricoType: String, identifierValue: Literal) {
        val newIdentifier = model.createResource()
        newIdentifier.addProperty(RDF.type, RICO.Identifier)
        newIdentifier.addLiteral(RICO.type, ricoType)
        newIdentifier.addLiteral(RICO.identifier, identifierValue)
        newIdentifier.addProperty(RICO.isOrWasIdentifierOf, resource)
        resource.addProperty(RICO.hasOrHadIdentifier, newIdentifier)
    }

    protected fun addMainIdentifier(identifierValue: Literal) {
        addIdentifier(RICO.Types.Identifier.main, identifierValue)
    }

    fun addLiteral(property: Property, values: List<Literal>) {
        values.forEach {
            resource.addLiteral(property, it)
        }
    }

    fun addLiteral(property: Property, literal: Literal) {
        resource.addLiteral(property, literal)
    }

    override fun addDate(property: Property, value: String) {
        val blank = model.createResource()
        blank.addProperty(RDF.type, RICO.DateSet)
        blank.addProperty(RICO.expressedDate, literal(value))
        resource.addProperty(property, blank)
    }

    override fun langLiteral(text: String, language: String): Literal = model.createLiteral(text.trim(), language)
    override fun literal(text: String): Literal = model.createLiteral(text.trim())

    protected fun uri(ns: String, name: String): String = ns + name
    private fun institutionUri(id: String): Resource = model.createResource(uri(NS.mbcb, id))
}
