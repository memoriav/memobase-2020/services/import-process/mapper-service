/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.builder

import ch.memobase.helpers.StringHelpers
import ch.memobase.rdf.*
import ch.memobase.schema.AgentMetadata
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Resource

class Record(
    sourceId: String,
    type: String,
    recordSetId: String,
    institutionId: String,
    hasSponsoringAgent: Boolean,
    isPublished: Boolean
) : RecordResource(institutionId) {

    private val id = recordSetId + "-" + StringHelpers.normalizeId(sourceId)
    val uri = NS.mbr + id
    override val resource: Resource = model.createResource(uri)

    init {
        addRdfType(RICO.Record)
        resource.addProperty(RICO.type, type)
        resource.addProperty(RICO.isOrWasPartOf, recordSetUri(recordSetId))
        resource.addProperty(RICO.hasOrHadHolder, institutionUri)

        addMainIdentifier(literal(id))
        if (hasSponsoringAgent) {
            resource.addProperty(RDA.hasSponsoringAgentOfResource, model.createResource(MB.memoriavInstitutionURI))
        }
        resource.addProperty(MB.isPublished, model.createTypedLiteral(isPublished))
    }

    fun addInstantiation(instantiation: Instantiation) {
        resource.addProperty(RICO.hasInstantiation, instantiation.resource)
    }

    fun addOriginalIdentifier(identifierValue: Literal) {
        addIdentifier(RICO.Types.Identifier.original, identifierValue)
    }

    fun addOldMemobaseIdentifier(identifierValue: Literal) {
        addIdentifier(RICO.Types.Identifier.oldMemobase, identifierValue)
    }

    private fun addTitle(ricoType: String, titles: List<Literal>) {
        if (titles.isEmpty()) return
        val newTitle = model.createResource()
        newTitle.addProperty(RDF.type, RICO.Title)
        newTitle.addLiteral(RICO.type, ricoType)
        titles.forEach {
            newTitle.addLiteral(RICO.title, it)
        }
        newTitle.addProperty(RICO.isOrWasTitleOf, resource)
        resource.addProperty(RICO.hasOrHadTitle, newTitle)
    }

    fun addMainTitle(titles: List<Literal>) {
        if (titles.isEmpty()) return

        titles.forEach {
            resource.addLiteral(RICO.title, it)
        }

        addTitle(RICO.Types.Title.main, titles)
    }

    fun addSeriesTitle(titles: List<Literal>) {
        addTitle(RICO.Types.Title.series, titles)
    }

    fun addBroadcastTitle(titles: List<Literal>) {
        addTitle(RICO.Types.Title.broadcast, titles)
    }

    fun addGenre(prefLabels: List<Literal>) {
        if (prefLabels.isEmpty()) return
        val newSubject = model.createResource()
        newSubject.addProperty(RDF.type, SKOS.Concept)
        prefLabels.forEach {
            newSubject.addLiteral(SKOS.prefLabel, it)
        }
        resource.addProperty(EBUCORE.hasGenre, newSubject)
    }

    fun addSubject(prefLabels: List<Literal>) {
        if (prefLabels.isEmpty()) return
        val newSubject = model.createResource()
        newSubject.addProperty(RDF.type, SKOS.Concept)
        prefLabels.forEach {
            newSubject.addLiteral(SKOS.prefLabel, it)
        }
        newSubject.addProperty(RICO.isOrWasSubjectOf, resource)
        resource.addProperty(RICO.hasOrHadSubject, newSubject)
    }

    fun addHolderRule(names: List<Literal>) {
        if (names.isEmpty()) {
            return
        }
        val newRule = model.createResource()
        newRule.addProperty(RDF.type, RICO.Rule)
        newRule.addLiteral(RICO.type, RICO.Types.Rule.holder)
        names.forEach {
            newRule.addProperty(RICO.name, it)
        }
        newRule.addProperty(RICO.regulatesOrRegulated, resource)
        resource.addProperty(RICO.isOrWasRegulatedBy, newRule)
    }

    private fun addLanguage(names: List<Literal>, type: String) {
        if (names.isEmpty()) {
            return
        }
        val newLanguage = model.createResource()
        newLanguage.addProperty(RDF.type, RICO.Language)
        newLanguage.addLiteral(RICO.type, type)
        names.forEach {
            newLanguage.addProperty(RICO.name, it)
        }
        newLanguage.addProperty(RICO.isOrWasLanguageOf, resource)
        resource.addProperty(RICO.hasOrHadLanguage, newLanguage)
    }

    fun addContentLanguage(names: List<Literal>) {
        addLanguage(names, RICO.Types.Language.content)
    }

    fun addCaptionLanguage(names: List<Literal>) {
        addLanguage(names, RICO.Types.Language.caption)
    }

    private fun createPlace(names: List<Literal>, sameAs: List<Literal>, coordinates: Literal?): Resource {
        val newPlace = model.createResource()
        newPlace.addProperty(RDF.type, RICO.Place)

        names.forEach {
            newPlace.addProperty(RICO.name, it)
        }
        sameAs.forEach {
            newPlace.addProperty(SCHEMA.sameAs, it)
        }
        if (coordinates != null) {
            newPlace.addLiteral(WD.coordinateLocation, coordinates)
        }

        return newPlace
    }

    fun addPlaceOfCapture(names: List<Literal>, sameAs: List<Literal>, coordinates: Literal?) {
        if (names.isEmpty()) return
        val newPlace = createPlace(names, sameAs, coordinates)

        newPlace.addProperty(RDA.isPlaceOfCaptureOf, resource)
        resource.addProperty(RDA.hasPlaceOfCapture, newPlace)
    }

    fun addRelatedPlaces(names: List<Literal>, sameAs: List<Literal>, coordinates: Literal?) {
        if (names.isEmpty()) return
        val newPlace = createPlace(names, sameAs, coordinates)

        resource.addProperty(DC.spatial, newPlace)
    }

    private fun addCreationRelation(names: List<Literal>, type: String, target: Resource) {
        val newCreationRelation = model.createResource()
        newCreationRelation.addProperty(RDF.type, RICO.CreationRelation)
        newCreationRelation.addLiteral(RICO.type, type)
        names.forEach {
            newCreationRelation.addLiteral(RICO.name, it)
        }
        newCreationRelation.addProperty(RICO.creationRelationHasSource, resource)
        resource.addProperty(RICO.recordResourceOrInstantiationIsSourceOfCreationRelation, newCreationRelation)
        newCreationRelation.addProperty(RICO.creationRelationHasTarget, target)
        target.addProperty(RICO.agentIsTargetOfCreationRelation, newCreationRelation)
    }

    private fun addAgent(
        rdfType: Resource,
        agentMetadata: AgentMetadata,
    ): Resource {
        val newAgent = model.createResource()
        newAgent.addProperty(RDF.type, rdfType)
        agentMetadata.name.forEach {
            newAgent.addLiteral(RICO.name, it)
        }

        agentMetadata.sameAs?.forEach {
            newAgent.addLiteral(SCHEMA.sameAs, it)
        }

        agentMetadata.hasVariantNameOfAgent?.forEach {
            newAgent.addLiteral(RDA.hasVariantNameOfAgent, it)
        }

        agentMetadata.gender?.let {
            newAgent.addLiteral(FOAF.gender, it)
        }

        agentMetadata.history?.forEach {
            newAgent.addLiteral(RICO.history, it)
        }

        agentMetadata.isMemberOf?.forEach {
            newAgent.addLiteral(RDA.isMemberOf, it)
        }

        agentMetadata.descriptiveNote?.forEach {
            newAgent.addLiteral(RICO.descriptiveNote, it)
        }

        agentMetadata.hasProfessionOrOccupation?.forEach {
            newAgent.addLiteral(RDA.hasProfessionOrOccupation, it)
        }

        fun addBirthDate(value: String) {
            val date = model.createResource()
            date.addProperty(RDF.type, RICO.DateSet)
            date.addProperty(RICO.expressedDate, literal(value))
            newAgent.addProperty(RICO.hasBirthDate, date)
            date.addProperty(RICO.isBirthDateOf, newAgent)
        }

        fun addDeathDate(value: String) {
            val date = model.createResource()
            date.addProperty(RDF.type, RICO.DateSet)
            date.addProperty(RICO.expressedDate, literal(value))
            newAgent.addProperty(RICO.hasDeathDate, date)
            date.addProperty(RICO.isDeathDateOf, newAgent)
        }

        agentMetadata.hasBirthDate?.let {
            addBirthDate(it)
        }

        agentMetadata.hasDeathDate?.let {
            addDeathDate(it)
        }

        agentMetadata.hasPeriodOfActivity?.forEach {
            addDate(RDA.hasPeriodOfActivityOfAgent, it)
        }

        return newAgent
    }


    fun addCreator(
        rdfType: Resource,
        agentMetadata: AgentMetadata,
    ) {
        val newAgent = addAgent(rdfType, agentMetadata)
        agentMetadata.relationNames.forEach {
            addCreationRelation(it, RICO.Types.CreationRelation.creator, newAgent)
        }

        if (agentMetadata.relationNames.isEmpty())
            addCreationRelation(emptyList(), RICO.Types.CreationRelation.creator, newAgent)
    }

    fun addContributor(
        rdfType: Resource,
        agentMetadata: AgentMetadata,
    ) {
        val newAgent = addAgent(rdfType, agentMetadata)
        agentMetadata.relationNames.forEach {
            addCreationRelation(it, RICO.Types.CreationRelation.contributor, newAgent)
        }

        if (agentMetadata.relationNames.isEmpty())
            addCreationRelation(emptyList(), RICO.Types.CreationRelation.contributor, newAgent)
    }

    fun addPublisher(
        rdfType: Resource,
        agentMetadata: AgentMetadata,
    ) {
        val newAgent = addAgent(rdfType, agentMetadata)
        newAgent.addProperty(RICO.isPublisherOf, resource)
        resource.addProperty(RICO.hasPublisher, newAgent)
    }

    fun addProducer(rdfType: Resource, agentMetadata: AgentMetadata) {
        val newAgent = addAgent(rdfType, agentMetadata)
        newAgent.addProperty(RDA.isProducerOf, resource)
        resource.addProperty(RDA.hasProducer, newAgent)
    }

    fun addSubjectAgent(rdfType: Resource, agentMetadata: AgentMetadata) {
        val newAgent = addAgent(rdfType, agentMetadata)
        newAgent.addProperty(RICO.isOrWasSubjectOf, resource)
        resource.addProperty(RICO.hasOrHadSubject, newAgent)
    }


    private fun recordSetUri(id: String): Resource = model.createResource(uri(NS.mbrs, id))
}
