/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.builder

import ch.memobase.helpers.StringHelpers
import ch.memobase.rdf.NS
import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import org.apache.jena.rdf.model.Literal
import org.apache.jena.rdf.model.Resource

class PhysicalObject(sourceId: String, recordSetId: String, institutionId: String, count: Int) :
    Instantiation(institutionId) {
    private val id = recordSetId + "-" + StringHelpers.normalizeId(sourceId) + "-" + count
    override val resource: Resource = model.createResource(NS.mbpo + id)

    init {
        addRdfType(RICO.Instantiation)
        resource.addProperty(RICO.type, RICO.Types.Instantiation.physicalObject)
        addMainIdentifier(literal(id))
    }

    fun addCallNumber(value: Literal) {
        val newIdentifier = model.createResource()
        newIdentifier.addProperty(RDF.type, RICO.Identifier)
        newIdentifier.addLiteral(RICO.type, RICO.Types.Identifier.callNumber)
        newIdentifier.addLiteral(RICO.identifier, value)
        newIdentifier.addProperty(RICO.isOrWasIdentifierOf, resource)
        resource.addProperty(RICO.hasOrHadIdentifier, newIdentifier)
    }

    fun addCarrierType(names: List<Literal>) {
        if (names.isEmpty()) return
        val carrierType = model.createResource()
        carrierType.addProperty(RDF.type, RICO.CarrierType)
        names.forEach { carrierType.addLiteral(RICO.name, it) }
        resource.addProperty(RICO.hasCarrierType, carrierType)
        carrierType.addProperty(RICO.isCarrierTypeOf, resource)
    }
}
