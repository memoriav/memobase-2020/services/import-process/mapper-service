/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.builder

import ch.memobase.rdf.RDF
import ch.memobase.rdf.RICO
import ch.memobase.rdf.SCHEMA
import org.apache.jena.rdf.model.Literal

abstract class Instantiation(institutionId: String) : RecordResource(institutionId) {
    fun addRecord(record: Record) {
        resource.addProperty(RICO.isInstantiationOf, record.resource)
    }

    fun addDerivedInstantiation(instantiation: Instantiation) {
        resource.addProperty(RICO.hasDerivedInstantiation, instantiation.resource)
    }

    fun addIsDerivedFromInstantiation(instantiation: Instantiation) {
        resource.addProperty(RICO.isDerivedFromInstantiation, instantiation.resource)
    }

    private fun addRule(names: List<Literal>, ruleType: String, sameAs: List<Literal> = emptyList()) {
        if (names.isEmpty()) return
        val rule = model.createResource().apply {
            addProperty(RDF.type, RICO.Rule)
            addLiteral(RICO.type, ruleType)
            names.forEach { addLiteral(RICO.name, it) }
            sameAs.forEach { addLiteral(SCHEMA.sameAs, it) }
            addProperty(RICO.regulatesOrRegulated, resource)
        }
        resource.addProperty(RICO.isOrWasRegulatedBy, rule)
    }

    fun addAccessRule(names: List<Literal>) {
        addRule(names, RICO.Types.Rule.access)
    }

    fun addUsageRule(names: List<Literal>, sameAs: List<Literal>) {
        addRule(names, RICO.Types.Rule.usage, sameAs)
    }

}
