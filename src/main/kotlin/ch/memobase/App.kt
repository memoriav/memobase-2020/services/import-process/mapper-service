/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.PropertyName.APP_VERSION
import ch.memobase.helpers.PropertyName.CONFIG_TOPIC
import ch.memobase.helpers.PropertyName.STEP_NAME
import ch.memobase.impl.Server
import ch.memobase.settings.SettingsLoader
import org.apache.logging.log4j.LogManager
import kotlin.system.exitProcess

class App {
    companion object {
        private val log = LogManager.getLogger(this::class.java)
        @JvmStatic fun main(args: Array<String>) {
            Server.startServer()
            try {
                Service.run()
            } catch (ex: Exception) {
                ex.printStackTrace()
                log.error("Stopping application due to error: " + ex.message)
                exitProcess(1)
            }
        }

        fun defineSettings(fileName: String): SettingsLoader {
            return SettingsLoader(
                listOf(
                    STEP_NAME,
                    CONFIG_TOPIC,
                    APP_VERSION,
                ),
                fileName,
                useStreamsConfig = true
            )
        }
    }
}
