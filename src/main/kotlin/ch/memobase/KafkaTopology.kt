/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.helpers.BranchName.SK_EXCEPTION
import ch.memobase.helpers.BranchName.SK_EXCEPTION_NO
import ch.memobase.helpers.BranchName.SK_EXCEPTION_YES
import ch.memobase.helpers.BranchName.SK_PARSE
import ch.memobase.helpers.BranchName.SK_PARSE_FATAL
import ch.memobase.helpers.BranchName.SK_PARSE_SUCCESS
import ch.memobase.helpers.PropertyName.APP_VERSION
import ch.memobase.helpers.PropertyName.CONFIG_TOPIC
import ch.memobase.helpers.PropertyName.STEP_NAME
import ch.memobase.helpers.Reports
import ch.memobase.impl.Parsers
import ch.memobase.impl.ResourceBuilder
import ch.memobase.kafka.utils.ConfigJoiner
import ch.memobase.kafka.utils.models.ImportService
import ch.memobase.kafka.utils.models.JoinedValues
import ch.memobase.kafka.utils.models.ValueWithException
import ch.memobase.reporting.Report
import ch.memobase.reporting.ReportStatus
import ch.memobase.schema.PipelineData
import ch.memobase.schema.PipelineDataParsed
import ch.memobase.schema.ResourceBuilderRenderedOutput
import ch.memobase.settings.HeaderExtractionSupplier
import ch.memobase.settings.HeaderMetadata
import ch.memobase.settings.SettingsLoader
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.buildJsonObject
import org.apache.jena.riot.RDFFormat
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.kstream.Branched
import org.apache.kafka.streams.kstream.KStream
import org.apache.kafka.streams.kstream.Named
import org.apache.logging.log4j.LogManager

class KafkaTopology(
    private val settings: SettingsLoader,
) {

    private val log = LogManager.getLogger(this::class.java)
    private val reportTopic = settings.processReportTopic

    private val reports = Reports(
        settings.appSettings.getProperty(STEP_NAME),
        settings.appSettings.getProperty(APP_VERSION)
    )

    private val configJoiner = ConfigJoiner(
        ImportService.Mapping,
        Serdes.String(),
        /**
         * Serde as Bytearray as this transformation happens a few times, but doesn't really add anything.
         */
        Serdes.serdeFrom(
            { _, value ->
                value
            },
            { _, value ->
                Parsers.parseConfig(value)
            }
        ),
        Parsers::parseConfig
    )

    fun prepare(): StreamsBuilder {
        val builder = StreamsBuilder()

        val configStream = builder.stream<String, String>(settings.appSettings.getProperty(CONFIG_TOPIC))
            .map { key, value -> KeyValue(key.toByteArray(), value.toByteArray()) }

        val stream = builder.stream<String, String>(settings.inputTopic)

        val joinedStream =
            configJoiner.join(stream, configStream)

        val handledStream = joinedStream
            .mapValues { value -> handleExceptions(value) }
            .split(Named.`as`(SK_EXCEPTION))
            .branch(
                { _, value -> value.exception.isNotEmpty() },
                Branched.`as`(SK_EXCEPTION_YES)
            )
            .defaultBranch(Branched.`as`(SK_EXCEPTION_NO))

        handledStream[SK_EXCEPTION + SK_EXCEPTION_YES]
            ?.mapValues { readOnlyKey, value ->
                val report = reports.fatal(readOnlyKey, "CONFIGURATION ERROR: ${value.exception}")
                log.error(report)
                report.toJson()
            }
            ?.to(reportTopic)

        val parsedStream = handledStream[SK_EXCEPTION + SK_EXCEPTION_NO]
            ?.mapValues { readOnlyKey, value ->
                try {
                    val parsedInput = Parsers.parseInput(value.input)
                    PipelineDataParsed(
                        parsedInput,
                        value.config!!,
                        null
                    )
                } catch (ex: SerializationException) {
                    PipelineDataParsed(
                        buildJsonObject {  },
                        value.config!!,
                        reports.fatal(readOnlyKey, ex.stackTraceToString())
                    )
                } catch (ex: IllegalArgumentException) {
                    PipelineDataParsed(
                        buildJsonObject {  },
                        value.config!!,
                        reports.fatal(readOnlyKey, ex.stackTraceToString())
                    )
                } catch (ex: IllegalArgumentException) {
                    PipelineDataParsed(
                        buildJsonObject {  },
                        value.config!!,
                        reports.fatal(readOnlyKey, ex.stackTraceToString())
                    )
                }
            }
            ?.split(Named.`as`(SK_PARSE))
            ?.branch(
                { _, value -> value.report?.status == ReportStatus.fatal },
                Branched.`as`(SK_PARSE_FATAL)
            )
            ?.defaultBranch(Branched.`as`(SK_PARSE_SUCCESS))

        parsedStream?.get(SK_PARSE + SK_PARSE_FATAL)
            ?.mapValues { value ->
                val report = value.report
                log.error(report)
                value.report?.toJson() }
            ?.to(reportTopic)

        val initResourceBuilder = parsedStream?.get(SK_PARSE + SK_PARSE_SUCCESS)
            ?.processValues(HeaderExtractionSupplier<PipelineDataParsed>())
            ?.mapValues { readOnlyKey, value -> buildResources(readOnlyKey, value) }

        initResourceBuilder
            ?.filter { _, value -> value.second.status == ReportStatus.fatal }
            ?.mapValues { value -> value.second.toJson() }
            ?.to(reportTopic)

        val mapResources = initResourceBuilder
            ?.filter { _, value -> value.second.status != ReportStatus.fatal }
            ?.mapValues { value -> value.first!! }
            ?.map { _, value -> value.generateRecord() }
            ?.mapValues { value -> value.resourceBuilder.generatePhysicalObject(value.resources, value.report) }
            ?.mapValues { value -> value.resourceBuilder.generateDigitalObject(value.resources, value.report) }
            ?.mapValues { value -> value.resourceBuilder.generateThumbnailObject(value.resources, value.report) }

        val recordStream = mapResources
            ?.mapValues { _, value ->
                val result = value.resourceBuilder.writeRecord(
                    RDFFormat.NTRIPLES_UTF8,
                    value.resources,
                    value.report
                )
                log.debug(result.content)
                result
            }

        objectOutput(recordStream)

        return builder
    }

    private fun handleExceptions(value: ValueWithException<JoinedValues<String, ByteArray>>): PipelineData {
        return when {
            value.hasException() -> {
                PipelineData("", null, value.exception.localizedMessage)
            }

            value.hasValue() -> {
                val configData = value.value.right
                if (configData == null) {
                    PipelineData("", null, "Mapper configuration was not found.")
                } else {
                    val result = Parsers.parseConfigInPipeline(configData)
                    if (result.first == null) {
                        PipelineData("", null, result.second)
                    } else {
                        PipelineData(value.value.left, result.first, "")
                    }
                }
            }

            else -> {
                PipelineData("", null, "Could not handle error in kafka utils library.")
            }
        }
    }

    private fun objectOutput(stream: KStream<String, ResourceBuilderRenderedOutput>?) {
        stream
            ?.filter { _, value -> value.report.status != ReportStatus.fatal }
            ?.mapValues { _, value -> value.content }
            ?.to(settings.outputTopic)

        stream
            ?.mapValues { _, value -> value.report.toJson() }
            ?.to(reportTopic)

        stream?.foreach { key, value ->
            log.debug("Key: $key")
            log.debug("Value: ${value.content}")
            log.debug("Report: ${value.report}")
        }
    }

    private fun buildResources(
        key: String,
        value: Pair<PipelineDataParsed, HeaderMetadata>
    ): Pair<ResourceBuilder?, Report> {
        return ResourceBuilder.initializeBuilder(
            key,
            value.first.input,
            value.first.config,
            reports,
            value.second.institutionId,
            value.second.recordSetId,
            value.second.isPublished
        )
    }
}
