/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.CONST
import ch.memobase.helpers.YamlKeys.DE
import ch.memobase.helpers.YamlKeys.FIELD
import ch.memobase.helpers.YamlKeys.FR
import ch.memobase.helpers.YamlKeys.IT
import ch.memobase.helpers.YamlKeys.PARENT_FIELD
import ch.memobase.helpers.YamlKeys.PREFIX
import ch.memobase.helpers.YamlKeys.validMultiLangFieldMapKeys
import ch.memobase.schema.mapping.SerializerUtils.extractField
import ch.memobase.schema.mapping.SerializerUtils.extractPrefixNode
import ch.memobase.schema.mapping.SerializerUtils.mapToScalarNode
import ch.memobase.schema.mapping.SerializerUtils.yaml
import com.charleskorn.kaml.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object MultiLangFieldSerializer : KSerializer<MultiLangField> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(MultiLangField::class.java.name) {
        element<String>(FIELD)
        element<PrefixData>(PREFIX)
        element<String>(CONST)
        element<Field>(DE)
        element<Field>(FR)
        element<Field>(IT)
    }

    override fun deserialize(decoder: Decoder): MultiLangField {
        decoder as YamlInput
        return when (val node = decoder.node) {
            is YamlList -> throw SerializationException("Expected field node, found list instead: ${node.path.toHumanReadableString()}.")
            is YamlMap -> {
                node.entries.keys.forEach {
                    if (!validMultiLangFieldMapKeys.contains(it.content))
                        throw SerializationException("Unknown label ${it.content} at path ${node.path.toHumanReadableString()}.")
                }

                val parentField = mapToScalarNode(node, PARENT_FIELD)
                val field = mapToScalarNode(node, FIELD)
                val constant = mapToScalarNode(node, CONST)
                val prefix = extractPrefixNode(node)
                val deField = extractField(node, DE)
                val frField = extractField(node, FR)
                val itField = extractField(node, IT)

                if (listOfNotNull(parentField, field, constant, prefix, deField, frField, itField).size > 3)
                    throw SerializationException("Too many fields defined in field: ${node.path.toHumanReadableString()}.")

                return if (parentField != null && field != null)
                    MapFieldWithParent(parentField, field)
                else if (field != null)
                    MapField(field)
                else if (parentField != null && prefix != null)
                    PrefixFieldWithParent(parentField, yaml.decodeFromYamlNode(PrefixData.serializer(), prefix))
                else if (prefix != null)
                    PrefixField(yaml.decodeFromYamlNode(PrefixData.serializer(), prefix))
                else if (constant != null)
                    ConstantField(constant)
                else if (deField != null && frField != null && itField != null)
                    LanguageField(deField, frField, itField)
                else if (deField != null && frField != null)
                    GermanFrenchField(deField, frField)
                else if (deField != null && itField != null)
                    GermanItalianField(deField, itField)
                else if (frField != null && itField != null)
                    FrenchItalianField(frField, itField)
                else if (deField != null)
                    GermanField(deField)
                else if (frField != null)
                    FrenchField(frField)
                else if (itField != null)
                    ItalianField(itField)
                else
                    throw SerializationException("Unsupported configuration detected: ${node.path.toHumanReadableString()}")

            }
            is YamlNull -> throw SerializationException("Expected field node, found null instead: ${node.path.toHumanReadableString()}.")
            is YamlScalar -> MapField(
                name = node.content
            )
            is YamlTaggedNode -> throw SerializationException("Expected field node, found tagged node instead: ${node.path.toHumanReadableString()}.")
        }
    }

    override fun serialize(encoder: Encoder, value: MultiLangField) {
        throw UnsupportedOperationException()
    }
}