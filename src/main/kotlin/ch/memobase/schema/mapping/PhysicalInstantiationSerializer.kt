/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.CARRIER_TYPE
import ch.memobase.helpers.YamlKeys.COLOUR
import ch.memobase.helpers.YamlKeys.CONDITIONS_OF_ACCESS
import ch.memobase.helpers.YamlKeys.CONDITIONS_OF_USE
import ch.memobase.helpers.YamlKeys.DESCRIPTIVE_NOTE
import ch.memobase.helpers.YamlKeys.DURATION
import ch.memobase.helpers.YamlKeys.IDENTIFIERS
import ch.memobase.helpers.YamlKeys.PHYSICAL
import ch.memobase.helpers.YamlKeys.PHYSICAL_CHARACTERISTICS
import ch.memobase.helpers.YamlKeys.RIGHTS
import ch.memobase.schema.mapping.SerializerUtils.decodeOneMap
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangFields
import ch.memobase.schema.mapping.SerializerUtils.getScalar
import ch.memobase.schema.mapping.SerializerUtils.missingField
import com.charleskorn.kaml.YamlInput
import com.charleskorn.kaml.YamlMap
import com.charleskorn.kaml.yamlMap
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object PhysicalInstantiationSerializer : KSerializer<PhysicalInstantiation> {

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(PhysicalInstantiation::class.java.name) {
        element(CARRIER_TYPE, ListSerializer(MultiLangField.serializer()).descriptor)
        element(PHYSICAL_CHARACTERISTICS, ListSerializer(MultiLangField.serializer()).descriptor)
        element(DESCRIPTIVE_NOTE, ListSerializer(MultiLangField.serializer()).descriptor)
        element(COLOUR, ListSerializer(MultiLangField.serializer()).descriptor)
        element(CONDITIONS_OF_ACCESS, ListSerializer(MultiLangField.serializer()).descriptor)
        element(CONDITIONS_OF_USE, ListSerializer(MultiLangField.serializer()).descriptor)
        element<String>(DURATION)
        element<PhysicalIdentifiers>(IDENTIFIERS)
        element<PhysicalRights>(RIGHTS)
    }

    override fun deserialize(decoder: Decoder): PhysicalInstantiation {
        decoder as YamlInput
        val values = decoder.node.yamlMap
        val physical = values.get<YamlMap>(PHYSICAL) ?: missingField(PHYSICAL, PhysicalInstantiation::class.java.name)

        listOf(
            CARRIER_TYPE,
            DURATION,
            PHYSICAL_CHARACTERISTICS,
            DESCRIPTIVE_NOTE,
            COLOUR,
            IDENTIFIERS,
            RIGHTS,
            CONDITIONS_OF_USE,
            CONDITIONS_OF_ACCESS
        ).let { list ->
            physical.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found under physical: ${entry.content}.")
                }
            }
        }
        return PhysicalInstantiation(
            carrierType = extractMultiLangFields(physical, CARRIER_TYPE),
            duration = getScalar(physical, DURATION),
            physicalCharacteristics = extractMultiLangFields(physical, PHYSICAL_CHARACTERISTICS),
            descriptiveNote = extractMultiLangFields(physical, DESCRIPTIVE_NOTE),
            colour = extractMultiLangFields(physical, COLOUR),
            identifiers = decodeOneMap<PhysicalIdentifiers>(physical, IDENTIFIERS, PhysicalIdentifiers.serializer()),
            rights = decodeOneMap<PhysicalRights>(physical, RIGHTS, PhysicalRights.serializer()) ?: missingField(
                RIGHTS,
                PhysicalInstantiation::class.java.name
            ),
            conditionsOfUse = extractMultiLangFields(physical, CONDITIONS_OF_USE),
            conditionsOfAccess = extractMultiLangFields(physical, CONDITIONS_OF_ACCESS),
        )
    }

    override fun serialize(encoder: Encoder, value: PhysicalInstantiation) {
        throw UnsupportedOperationException()
    }
}