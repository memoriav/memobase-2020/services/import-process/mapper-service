/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.ABSTRACT
import ch.memobase.helpers.YamlKeys.CONDITIONS_OF_ACCESS
import ch.memobase.helpers.YamlKeys.CONDITIONS_OF_USE
import ch.memobase.helpers.YamlKeys.CONST
import ch.memobase.helpers.YamlKeys.CONTRIBUTORS
import ch.memobase.helpers.YamlKeys.CREATION_DATE
import ch.memobase.helpers.YamlKeys.CREATORS
import ch.memobase.helpers.YamlKeys.DESCRIPTIVE_NOTE
import ch.memobase.helpers.YamlKeys.GENRE
import ch.memobase.helpers.YamlKeys.IDENTIFIERS
import ch.memobase.helpers.YamlKeys.ISSUED_DATE
import ch.memobase.helpers.YamlKeys.IS_SPONSORED_BY_MEMORIAV
import ch.memobase.helpers.YamlKeys.LANGUAGES
import ch.memobase.helpers.YamlKeys.PLACE_OF_CAPTURE
import ch.memobase.helpers.YamlKeys.PRODUCERS
import ch.memobase.helpers.YamlKeys.PUBLISHED_BY
import ch.memobase.helpers.YamlKeys.RECORD
import ch.memobase.helpers.YamlKeys.RELATED_AGENTS
import ch.memobase.helpers.YamlKeys.RELATED_PLACES
import ch.memobase.helpers.YamlKeys.RELATION
import ch.memobase.helpers.YamlKeys.RIGHTS
import ch.memobase.helpers.YamlKeys.SAME_AS
import ch.memobase.helpers.YamlKeys.SCOPE_AND_CONTENT
import ch.memobase.helpers.YamlKeys.SOURCE
import ch.memobase.helpers.YamlKeys.SUBJECT
import ch.memobase.helpers.YamlKeys.TEMPORAL
import ch.memobase.helpers.YamlKeys.TITLES
import ch.memobase.helpers.YamlKeys.TYPE
import ch.memobase.helpers.YamlKeys.URI
import ch.memobase.schema.mapping.SerializerUtils.decodeOneMap
import ch.memobase.schema.mapping.SerializerUtils.decodeOneOrMoreMaps
import ch.memobase.schema.mapping.SerializerUtils.extractFields
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangFields
import ch.memobase.schema.mapping.SerializerUtils.getScalar
import ch.memobase.schema.mapping.SerializerUtils.missingField
import com.charleskorn.kaml.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object RecordSerializer : KSerializer<Record> {
    private val yaml = Yaml.default

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(Record::class.java.name) {
        element<String>(URI)
        element<Boolean>(IS_SPONSORED_BY_MEMORIAV)
        element<Identifiers>(IDENTIFIERS)
        element<RecordTypeField>(TYPE)
        element(TITLES, ListSerializer(Title.serializer()).descriptor)
        element(SCOPE_AND_CONTENT, ListSerializer(MultiLangField.serializer()).descriptor)
        element(ABSTRACT, ListSerializer(MultiLangField.serializer()).descriptor)
        element(DESCRIPTIVE_NOTE, ListSerializer(MultiLangField.serializer()).descriptor)
        element(SOURCE, ListSerializer(MultiLangField.serializer()).descriptor)
        element(RELATION, ListSerializer(MultiLangField.serializer()).descriptor)
        element(CONDITIONS_OF_USE, ListSerializer(MultiLangField.serializer()).descriptor)
        element(CONDITIONS_OF_ACCESS, ListSerializer(MultiLangField.serializer()).descriptor)
        element<Field>(SAME_AS)
        element<Rights>(RIGHTS)
        element<Languages?>(LANGUAGES)
        element(SUBJECT, ListSerializer(Subject.serializer()).descriptor)
        element(GENRE, ListSerializer(Subject.serializer()).descriptor)
        element(PLACE_OF_CAPTURE, ListSerializer(Place.serializer()).descriptor)
        element(RELATED_PLACES, ListSerializer(Place.serializer()).descriptor)
        element<String>(CREATION_DATE)
        element<String>(ISSUED_DATE)
        element<String>(TEMPORAL)
        element(CREATORS, ListSerializer(Agent.serializer()).descriptor)
        element(CONTRIBUTORS, ListSerializer(Agent.serializer()).descriptor)
        element(PRODUCERS, ListSerializer(Agent.serializer()).descriptor)
        element(RELATED_AGENTS, ListSerializer(Agent.serializer()).descriptor)
        element(PUBLISHED_BY, ListSerializer(Agent.serializer()).descriptor)
    }

    override fun deserialize(decoder: Decoder): Record {
        decoder as YamlInput
        val values = decoder.node.yamlMap
        val record = values.get<YamlMap>(RECORD) ?: missingField(RECORD, Record::class.java.name)
        listOf(
            URI,
            IDENTIFIERS,
            TYPE,
            TITLES,
            SCOPE_AND_CONTENT,
            SAME_AS,
            ABSTRACT,
            DESCRIPTIVE_NOTE,
            SOURCE,
            RELATION,
            IS_SPONSORED_BY_MEMORIAV,
            RIGHTS,
            LANGUAGES,
            SUBJECT,
            GENRE,
            PLACE_OF_CAPTURE,
            RELATED_PLACES,
            CREATION_DATE,
            ISSUED_DATE,
            TEMPORAL,
            CREATORS,
            CONTRIBUTORS,
            PRODUCERS,
            RELATED_AGENTS,
            PUBLISHED_BY,
            CONDITIONS_OF_USE,
            CONDITIONS_OF_ACCESS,
        ).let { list ->
            record.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found under record: ${entry.content}.")
                }
            }
        }
        return Record(
            uri = getScalar(record, URI) ?: missingField(URI, Record::class.java.name),
            type = getRequiredRecordTypeField(record),
            isSponsoredByMemoriav = record.getScalar(IS_SPONSORED_BY_MEMORIAV)?.toBoolean() ?: false,
            identifiers = decodeOneMap<Identifiers>(record, IDENTIFIERS, Identifiers.serializer())
                ?: missingField(IDENTIFIERS, Record::class.java.name),
            titles = decodeOneOrMoreMaps<Title>(record, TITLES, TitleSerializer)
                ?: missingField(TITLES, Record::class.java.name),
            scopeAndContent = extractMultiLangFields(record, SCOPE_AND_CONTENT),
            abstract = extractMultiLangFields(record, ABSTRACT),
            descriptiveNote = extractMultiLangFields(record, DESCRIPTIVE_NOTE),
            source = extractMultiLangFields(record, SOURCE),
            relation = extractMultiLangFields(record, RELATION),
            conditionsOfUse = extractMultiLangFields(record, CONDITIONS_OF_USE),
            conditionsOfAccess = extractMultiLangFields(record, CONDITIONS_OF_ACCESS),
            sameAs = extractFields(record, SAME_AS),
            rights = decodeOneMap<Rights>(record, RIGHTS, Rights.serializer()),
            languages = decodeOneMap<Languages>(record, LANGUAGES, LanguagesSerializer),
            subject = decodeOneOrMoreMaps<Subject>(record, SUBJECT, Subject.serializer()),
            genre = decodeOneOrMoreMaps<Subject>(record, GENRE, Subject.serializer()),
            placeOfCapture = decodeOneOrMoreMaps<Place>(record, PLACE_OF_CAPTURE, Place.serializer()),
            relatedPlaces = decodeOneOrMoreMaps<Place>(record, RELATED_PLACES, Place.serializer()),
            creationDate = getScalar(record, CREATION_DATE),
            issuedDate = getScalar(record, ISSUED_DATE),
            temporal = getScalar(record, TEMPORAL),
            creators = decodeOneOrMoreMaps<Agent>(record, CREATORS, Agent.serializer()),
            contributors = decodeOneOrMoreMaps<Agent>(record, CONTRIBUTORS, Agent.serializer()),
            producers = decodeOneOrMoreMaps<Agent>(record, PRODUCERS, Agent.serializer()),
            relatedAgents = decodeOneOrMoreMaps<Agent>(record, RELATED_AGENTS, Agent.serializer()),
            publishedBy = decodeOneOrMoreMaps<Agent>(record, PUBLISHED_BY, Agent.serializer()),
        )
    }

    override fun serialize(encoder: Encoder, value: Record) {
        throw UnsupportedOperationException()
    }

    private fun getRequiredRecordTypeField(record: YamlMap): RecordTypeField {
        return when (val recordTypeField = record.get<YamlNode>(TYPE)) {
            is YamlMap -> recordTypeField.get<YamlScalar>(CONST)?.content.let {
                if (it != null) {
                    RecordTypeField(const = yaml.decodeFromString(it))
                } else {
                    throw UnsupportedOperationException("Unexpected record type field")
                }
            }

            is YamlScalar -> RecordTypeField(map = recordTypeField.content)
            else -> throw UnsupportedOperationException("Unexpected record type field")
        }
    }
}