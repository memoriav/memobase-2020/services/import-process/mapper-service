/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import kotlinx.serialization.Serializable

@Serializable(with = FieldSerializer::class)
sealed class Field : MultiLangField()

@Serializable
data class MapField(
    val name: String,
): Field()

@Serializable
sealed class FieldWithParent(val parentField: String) : Field()

@Serializable
data class MapFieldWithParent(
    val parent: String,
    val name: String,
): FieldWithParent(parent)

@Serializable
data class PrefixField(
    val prefix: PrefixData,
): Field()

@Serializable
data class PrefixFieldWithParent(
    val parent: String,
    val prefix: PrefixData,
): FieldWithParent(parent)

@Serializable
data class ConstantField(
    val value: String,
): Field()