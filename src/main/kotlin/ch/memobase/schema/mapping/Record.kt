/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import kotlinx.serialization.Serializable

@Serializable(with = RecordSerializer::class)
data class Record(
    val uri: String,
    val type: RecordTypeField,
    val isSponsoredByMemoriav: Boolean,
    val identifiers: Identifiers,
    val titles: List<Title>,
    val sameAs: List<Field>?,
    val scopeAndContent: List<MultiLangField>?,
    val abstract: List<MultiLangField>?,
    val descriptiveNote: List<MultiLangField>?,
    val source: List<MultiLangField>?,
    val relation: List<MultiLangField>?,
    val conditionsOfUse: List<MultiLangField>?,
    val conditionsOfAccess: List<MultiLangField>?,
    val rights: Rights?,
    val languages: Languages?,
    val subject: List<Subject>?,
    val genre: List<Subject>?,
    val placeOfCapture: List<Place>?,
    val relatedPlaces: List<Place>?,
    val creationDate: String?,
    val issuedDate: String?,
    val temporal: String?,
    val creators: List<Agent>?,
    val contributors: List<Agent>?,
    val producers: List<Agent>?,
    val relatedAgents: List<Agent>?,
    val publishedBy: List<Agent>?,
)