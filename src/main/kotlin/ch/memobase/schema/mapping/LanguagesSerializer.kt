/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.rdf.RICO
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangFields
import com.charleskorn.kaml.YamlInput
import com.charleskorn.kaml.yamlMap
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object LanguagesSerializer : KSerializer<Languages> {

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(Languages::class.java.name) {
        element(RICO.Types.Language.caption, ListSerializer(MultiLangField.serializer()).descriptor)
        element(RICO.Types.Language.content, ListSerializer(MultiLangField.serializer()).descriptor)
    }

    override fun deserialize(decoder: Decoder): Languages {
        decoder as YamlInput
        val values = decoder.node.yamlMap

        listOf(
            RICO.Types.Language.caption,
            RICO.Types.Language.content,
        ).let { list ->
            values.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found under record.languages: ${entry.content}.")
                }
            }
        }
        return Languages(
            caption = extractMultiLangFields(values, RICO.Types.Language.caption),
            content = extractMultiLangFields(values, RICO.Types.Language.content),
        )
    }

    override fun serialize(encoder: Encoder, value: Languages) {
        throw UnsupportedOperationException()
    }
}