/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.COORDINATES
import ch.memobase.helpers.YamlKeys.NAME
import ch.memobase.helpers.YamlKeys.PARENT_FIELD
import ch.memobase.helpers.YamlKeys.SAME_AS
import ch.memobase.schema.mapping.SerializerUtils.extractField
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangField
import ch.memobase.schema.mapping.SerializerUtils.getOneOrMoreScalars
import ch.memobase.schema.mapping.SerializerUtils.getScalar
import ch.memobase.schema.mapping.SerializerUtils.missingField
import com.charleskorn.kaml.YamlInput
import com.charleskorn.kaml.yamlMap
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object PlaceSerializer : KSerializer<Place> {

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(Place::class.java.name) {
        element<String>(PARENT_FIELD)
        element<MultiLangField>(NAME)
        element(SAME_AS, ListSerializer(Field.serializer()).descriptor)
        element<Field>(COORDINATES)
    }

    override fun deserialize(decoder: Decoder): Place {
        decoder as YamlInput
        val values = decoder.node.yamlMap
        listOf(
            PARENT_FIELD,
            NAME,
            SAME_AS,
            COORDINATES,
        ).let { list ->
            values.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found under place: ${entry.content}.")
                }
            }
        }
        return Place(
            parentField = getScalar(values, PARENT_FIELD),
            name = extractMultiLangField(values, NAME) ?: missingField(NAME, Place::class.java.name),
            sameAs = getOneOrMoreScalars(values, SAME_AS),
            coordinates = extractField(values, COORDINATES),
        )
    }

    override fun serialize(encoder: Encoder, value: Place) {
        throw UnsupportedOperationException()
    }
}