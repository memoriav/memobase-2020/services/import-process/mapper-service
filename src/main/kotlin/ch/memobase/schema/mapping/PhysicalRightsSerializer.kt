/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.rdf.RICO
import ch.memobase.schema.mapping.SerializerUtils.decodeOneMap
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangFields
import com.charleskorn.kaml.YamlInput
import com.charleskorn.kaml.yamlMap
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object PhysicalRightsSerializer : KSerializer<PhysicalRights> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(PhysicalRights::class.java.name) {
        element(RICO.Types.Rule.access, ListSerializer(MultiLangField.serializer()).descriptor)
        element<Usage>(RICO.Types.Rule.usage)
    }

    override fun deserialize(decoder: Decoder): PhysicalRights {
        decoder as YamlInput
        val values = decoder.node.yamlMap
        listOf(
            RICO.Types.Rule.access,
            RICO.Types.Rule.usage,
        ).let { list ->
            values.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found under physical.rights: ${entry.content}.")
                }
            }
        }
        return PhysicalRights(
            access = extractMultiLangFields(values, RICO.Types.Rule.access),
            usage = decodeOneMap<Usage>(values, RICO.Types.Rule.usage, Usage.serializer())
        )
    }

    override fun serialize(encoder: Encoder, value: PhysicalRights) {
        throw UnsupportedOperationException()
    }
}