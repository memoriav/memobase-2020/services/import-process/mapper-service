/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.AGENT
import ch.memobase.helpers.YamlKeys.CORPORATE_BODY
import ch.memobase.helpers.YamlKeys.DESCRIPTIVE_NOTE
import ch.memobase.helpers.YamlKeys.GENDER
import ch.memobase.helpers.YamlKeys.HAS_BIRTH_DATE
import ch.memobase.helpers.YamlKeys.HAS_DEATH_DATE
import ch.memobase.helpers.YamlKeys.HAS_PERIOD_OF_ACTIVITY
import ch.memobase.helpers.YamlKeys.HAS_PROFESSION_OR_OCCUPATION
import ch.memobase.helpers.YamlKeys.HAS_VARIANT_NAME_OF_AGENT
import ch.memobase.helpers.YamlKeys.HISTORY
import ch.memobase.helpers.YamlKeys.IS_MEMBER_OF
import ch.memobase.helpers.YamlKeys.NAME
import ch.memobase.helpers.YamlKeys.PARENT_FIELD
import ch.memobase.helpers.YamlKeys.PERSON
import ch.memobase.helpers.YamlKeys.RELATION_NAME
import ch.memobase.helpers.YamlKeys.SAME_AS
import ch.memobase.schema.mapping.SerializerUtils.extractField
import ch.memobase.schema.mapping.SerializerUtils.extractFields
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangField
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangFields
import ch.memobase.schema.mapping.SerializerUtils.getOneOrMoreScalars
import ch.memobase.schema.mapping.SerializerUtils.getScalar
import ch.memobase.schema.mapping.SerializerUtils.missingField
import com.charleskorn.kaml.YamlInput
import com.charleskorn.kaml.YamlMap
import com.charleskorn.kaml.yamlMap
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object AgentDataSerializer : KSerializer<AgentData> {

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(AgentData::class.java.name) {
        element<String>(PARENT_FIELD)
        element<Field>(NAME)
        element(RELATION_NAME, ListSerializer(Field.serializer()).descriptor)
        element(SAME_AS, ListSerializer(Field.serializer()).descriptor)
        element(HAS_VARIANT_NAME_OF_AGENT, ListSerializer(Field.serializer()).descriptor)
        element<Field>(GENDER)
        element(HISTORY, ListSerializer(Field.serializer()).descriptor)
        element(HAS_PERIOD_OF_ACTIVITY, ListSerializer(String.serializer()).descriptor)
        element<String>(HAS_BIRTH_DATE)
        element<String>(HAS_DEATH_DATE)
        element(IS_MEMBER_OF, ListSerializer(Field.serializer()).descriptor)
        element(DESCRIPTIVE_NOTE, ListSerializer(Field.serializer()).descriptor)
        element(HAS_PROFESSION_OR_OCCUPATION, ListSerializer(Field.serializer()).descriptor)
    }

    override fun deserialize(decoder: Decoder): AgentData {
        decoder as YamlInput
        val values = decoder.node.yamlMap

        val person = values.get<YamlMap>(PERSON)
        val corporateBody = values.get<YamlMap>(CORPORATE_BODY)
        val agent = values.get<YamlMap>(AGENT)

        var count = 0
        for (i in listOf(person, corporateBody, agent)) {
            if (i != null) {
                count++
            }
        }

        if (count > 1) {
            throw SerializationException("Expected only one of $PERSON, $CORPORATE_BODY or $AGENT but got $count of them instead.")
        }

        val parent = if (person != null) {
            person
        } else if (corporateBody != null) {
            corporateBody
        } else if (agent != null) {
            agent
        } else {
            throw SerializationException(
                "Expected one of $PERSON, $CORPORATE_BODY or $AGENT as a " +
                        "property in ${values.entries.keys.map { it.content }}."
            )
        }

        listOf(
            PARENT_FIELD,
            RELATION_NAME,
            NAME,
            SAME_AS,
            HAS_VARIANT_NAME_OF_AGENT,
            GENDER,
            HISTORY,
            HAS_PERIOD_OF_ACTIVITY,
            HAS_BIRTH_DATE,
            HAS_DEATH_DATE,
            IS_MEMBER_OF,
            DESCRIPTIVE_NOTE,
            HAS_PROFESSION_OR_OCCUPATION
        ).let { list ->
            parent.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found under agent.x: ${entry.content}.")
                }
            }
        }

        return AgentData(
            parentField = getScalar(parent, PARENT_FIELD),
            name = extractMultiLangField(parent, NAME) ?: missingField(
                NAME,
                AgentData::class.java.name
            ),
            relationName = extractMultiLangFields(parent, RELATION_NAME),
            sameAs = extractFields(parent, SAME_AS),
            hasVariantNameOfAgent = extractMultiLangFields(parent, HAS_VARIANT_NAME_OF_AGENT),
            gender = extractField(parent, GENDER),
            history = extractMultiLangFields(parent, HISTORY),
            hasPeriodOfActivity = getOneOrMoreScalars(parent, HAS_PERIOD_OF_ACTIVITY),
            hasBirthDate = getScalar(parent, HAS_BIRTH_DATE),
            hasDeathDate = getScalar(parent, HAS_DEATH_DATE),
            isMemberOf = extractMultiLangFields(parent, IS_MEMBER_OF),
            descriptiveNote = extractMultiLangFields(parent, DESCRIPTIVE_NOTE),
            hasProfessionOrOccupation = extractMultiLangFields(parent, HAS_PROFESSION_OR_OCCUPATION),
        )
    }

    override fun serialize(encoder: Encoder, value: AgentData) {
        throw UnsupportedOperationException()
    }
}