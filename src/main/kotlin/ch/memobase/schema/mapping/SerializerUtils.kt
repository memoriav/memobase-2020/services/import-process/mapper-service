/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.PREFIX
import com.charleskorn.kaml.*
import kotlinx.serialization.*

object SerializerUtils {
    val yaml = Yaml.default

    @OptIn(ExperimentalSerializationApi::class)
    fun missingField(fieldName: String, serialName: String): Nothing {
        throw MissingFieldException(fieldName, serialName)
    }

    fun getScalar(map: YamlMap, key: String): String? {
        return map.get<YamlScalar>(key)?.content
    }

    fun getOneOrMoreScalars(map: YamlMap, key: String): List<String>? {
        return when (val node = map.get<YamlNode>(key)) {
            is YamlScalar -> listOf(node.content)
            is YamlList -> {
                node.items.map { item ->
                    when (item) {
                        is YamlScalar -> item.content
                        else -> throw UnsupportedOperationException("Expected only strings in list for ${item.path.toHumanReadableString()}.")
                    }
                }
            }

            null -> null
            is YamlMap -> throw UnsupportedOperationException("Found map with fields for key $key at ${node.path.toHumanReadableString()}.")
            is YamlNull -> null
            is YamlTaggedNode -> throw UnsupportedOperationException("Tagged nodes are not supported at ${node.path.toHumanReadableString()}.")
        }
    }

    fun mapToScalarNode(node: YamlMap, field: String): String? = when (val n = node.get<YamlNode>(field)) {
        is YamlList -> throw SerializationException("Expected scalar, found list node instead: ${n.path.toHumanReadableString()}.")
        is YamlMap -> throw SerializationException("Expected scalar, found map node instead: ${n.path.toHumanReadableString()}.")
        is YamlNull -> throw SerializationException("Expected scalar, found null node instead: ${n.path.toHumanReadableString()}.")
        is YamlScalar -> n.content
        is YamlTaggedNode -> throw SerializationException("Expected scalar, found tagged node instead: ${n.path.toHumanReadableString()}.")
        null -> null
    }

    fun extractPrefixNode(node: YamlMap) = when (val prefix = node.get<YamlNode>(PREFIX)) {
        is YamlList -> throw SerializationException("Expected prefix data, found list node instead: ${prefix.path.toHumanReadableString()}.")
        is YamlMap -> prefix
        is YamlNull -> null
        is YamlScalar -> throw SerializationException("Expected prefix data, found scalar node instead: ${prefix.path.toHumanReadableString()}.")
        is YamlTaggedNode -> throw SerializationException("Expected prefix data, found tagged node instead: ${prefix.path.toHumanReadableString()}.")
        null -> null
    }

    fun extractField(currentNode: YamlMap, fieldName: String): Field? {
        return when (val node = currentNode.get<YamlNode>(fieldName)) {
            null -> null
            is YamlScalar -> MapField(node.content)
            else -> currentNode.get<YamlNode>(fieldName)?.let { yaml.decodeFromYamlNode(Field.serializer(), it) }
        }
    }

    fun extractMultiLangField(currentNode: YamlMap, fieldName: String): MultiLangField? {
        return when (val node = currentNode.get<YamlNode>(fieldName)) {
            null -> null
            is YamlScalar -> MapField(node.content)
            else -> currentNode.get<YamlNode>(fieldName)?.let { yaml.decodeFromYamlNode(MultiLangField.serializer(), it) }
        }
    }

    fun extractFields(parent: YamlMap, fieldName: String): List<Field>? {
        return when (val node = parent.get<YamlNode>(fieldName)) {
            is YamlScalar -> {
                val field = node.content
                if (field.contains("."))
                    throw SerializationException("Do not use dot notation in mapped field names ${node.content}: ${node.path.toHumanReadableString()}.")
                listOf(MapField(name = node.content))
            }
            is YamlList -> node.items.map { listItem ->
                when (listItem) {
                    is YamlScalar -> MapField(listItem.content)
                    else -> yaml.decodeFromYamlNode(Field.serializer(), listItem)
                }
            }.toList()

            is YamlMap -> listOf(yaml.decodeFromYamlNode(Field.serializer(), node))
            is YamlTaggedNode -> throw UnsupportedOperationException("Tagged nodes are not supported (${node.path.toHumanReadableString()}).")
            is YamlNull -> null
            null -> null
        }
    }

    fun extractMultiLangFields(parent: YamlMap, fieldName: String): List<MultiLangField>? {
        return when (val node = parent.get<YamlNode>(fieldName)) {
            is YamlScalar -> {
                val field = node.content
                if (field.contains("."))
                    throw SerializationException("Do not use dot notation in mapped field names ${node.content}: ${node.path.toHumanReadableString()}")
                listOf(MapField(name = node.content))
            }
            is YamlList -> node.items.mapNotNull { listItem ->
                when (listItem) {
                    is YamlScalar -> MapField(listItem.content)
                    is YamlList -> throw SerializationException("Does not support list in list: ${listItem.path.toHumanReadableString()}.")
                    is YamlMap -> yaml.decodeFromYamlNode(MultiLangField.serializer(), listItem)
                    is YamlNull -> null
                    is YamlTaggedNode -> throw SerializationException("Does not support tagged node in list: ${listItem.path.toHumanReadableString()}.")
                }
            }.toList()

            is YamlMap -> listOf(yaml.decodeFromYamlNode(MultiLangField.serializer(), node))
            null -> null
            is YamlNull -> null
            is YamlTaggedNode -> throw UnsupportedOperationException("Tagged nodes are not supported (${node.path.toHumanReadableString()}).")
        }
    }

    inline fun <reified T> decodeOneMap(
        map: YamlMap,
        key: String,
        deserializationStrategy: DeserializationStrategy<T>
    ): T? {
        return map.get<YamlNode>(key).let {
            if (it != null) {
                yaml.decodeFromYamlNode(
                    deserializationStrategy,
                    node = it
                )
            } else {
                null
            }
        }
    }

    inline fun <reified T> decodeOneOrMoreMaps(
        map: YamlMap,
        key: String,
        deserializationStrategy: KSerializer<T>
    ): List<T>? {
        return when (val node = map.get<YamlNode>(key)) {
            is YamlList -> node.items.map {
                yaml.decodeFromYamlNode(
                    deserializationStrategy,
                    node = it
                )
            }

            is YamlMap -> listOf(yaml.decodeFromYamlNode(deserializationStrategy, node as YamlNode))
            null -> null
            is YamlNull -> null
            is YamlScalar -> throw UnsupportedOperationException("Expected a map or list at path ${map.path.toHumanReadableString()}.")
            is YamlTaggedNode -> throw UnsupportedOperationException("Tagged nodes are not supported at ${node.path.toHumanReadableString()}.")
        }
    }
}