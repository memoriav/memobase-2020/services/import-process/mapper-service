/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.CONDITIONS_OF_ACCESS
import ch.memobase.helpers.YamlKeys.CONDITIONS_OF_USE
import ch.memobase.helpers.YamlKeys.CONST
import ch.memobase.helpers.YamlKeys.DESCRIPTIVE_NOTE
import ch.memobase.helpers.YamlKeys.DIGITAL
import ch.memobase.helpers.YamlKeys.DURATION
import ch.memobase.helpers.YamlKeys.LOCATOR
import ch.memobase.helpers.YamlKeys.PROXY_TYPE
import ch.memobase.helpers.YamlKeys.RIGHTS
import ch.memobase.helpers.YamlKeys.TYPE
import ch.memobase.schema.mapping.SerializerUtils.decodeOneMap
import ch.memobase.schema.mapping.SerializerUtils.extractField
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangFields
import ch.memobase.schema.mapping.SerializerUtils.getScalar
import ch.memobase.schema.mapping.SerializerUtils.missingField
import ch.memobase.schema.mapping.SerializerUtils.yaml
import com.charleskorn.kaml.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object DigitalInstantiationSerializer : KSerializer<DigitalInstantiation> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(DigitalInstantiation::class.java.name) {
        element<Field>(LOCATOR)
        element<DigitalRights>(RIGHTS)
        element<ProxyValue>(PROXY_TYPE)
        element<String>(DURATION)
        element(DESCRIPTIVE_NOTE, ListSerializer(MultiLangField.serializer()).descriptor)
        element(CONDITIONS_OF_USE, ListSerializer(MultiLangField.serializer()).descriptor)
        element(CONDITIONS_OF_ACCESS, ListSerializer(MultiLangField.serializer()).descriptor)
    }

    override fun deserialize(decoder: Decoder): DigitalInstantiation {
        decoder as YamlInput
        val values = decoder.node.yamlMap
        val digital = values.get<YamlMap>(DIGITAL) ?: missingField(DIGITAL, RecordMap::class.java.name)

        listOf(LOCATOR, RIGHTS, PROXY_TYPE, DURATION, DESCRIPTIVE_NOTE, CONDITIONS_OF_USE, CONDITIONS_OF_ACCESS).let { list ->
            digital.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found under digital: ${entry.content}.")
                }
            }
        }

        return DigitalInstantiation(
            locator = extractField(digital, LOCATOR),
            proxy = getProxyValueField(digital),
            duration = getScalar(digital, DURATION),
            descriptiveNote = extractMultiLangFields(digital, DESCRIPTIVE_NOTE),
            conditionsOfUse = extractMultiLangFields(digital, CONDITIONS_OF_USE),
            conditionsOfAccess = extractMultiLangFields(digital, CONDITIONS_OF_ACCESS),
            rights = decodeOneMap<DigitalRights>(digital, RIGHTS, DigitalRights.serializer())
                ?: missingField(RIGHTS, DigitalInstantiation::class.java.name),
        )
    }

    override fun serialize(encoder: Encoder, value: DigitalInstantiation) {
        throw UnsupportedOperationException()
    }

    private fun getProxyValueField(digital: YamlMap): ProxyField {
        return when (val proxyTypeField = digital.get<YamlNode>(TYPE)) {
            is YamlMap -> proxyTypeField.get<YamlScalar>(CONST)?.content.let {
                if (it != null) {
                    ProxyConstant(yaml.decodeFromString(it))
                } else {
                    throw UnsupportedOperationException("Unexpected proxy value field")
                }
            }

            is YamlScalar -> ProxyMap(proxyTypeField.content)
            null -> ProxyConstant(ProxyValue.proxydirect)
            else -> throw UnsupportedOperationException("Unexpected proxy value field")
        }
    }
}