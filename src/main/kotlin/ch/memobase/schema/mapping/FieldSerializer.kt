/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.CONST
import ch.memobase.helpers.YamlKeys.FIELD
import ch.memobase.helpers.YamlKeys.PARENT_FIELD
import ch.memobase.helpers.YamlKeys.PREFIX
import ch.memobase.helpers.YamlKeys.validFieldMapKeys
import ch.memobase.schema.mapping.SerializerUtils.extractPrefixNode
import ch.memobase.schema.mapping.SerializerUtils.mapToScalarNode
import ch.memobase.schema.mapping.SerializerUtils.yaml
import com.charleskorn.kaml.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object FieldSerializer : KSerializer<Field> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(Field::class.java.name) {
        element<String>(PARENT_FIELD)
        element<String>(FIELD)
        element<PrefixData>(PREFIX)
        element<String>(CONST)
    }

    override fun deserialize(decoder: Decoder): Field {
        decoder as YamlInput

        return when (val node = decoder.node) {
            is YamlList -> throw SerializationException("Expected field node, found list instead: ${node.path.toHumanReadableString()}.")
            is YamlMap -> {
                node.entries.keys.forEach {
                    if (!validFieldMapKeys.contains(it.content))
                        throw SerializationException("Unknown label ${it.content} at path ${node.path.toHumanReadableString()}.")
                }

                val parentField = mapToScalarNode(node, PARENT_FIELD)
                val prefix = extractPrefixNode(node)
                val field = mapToScalarNode(node, FIELD)
                val constant = mapToScalarNode(node, CONST)

                if (parentField != null) {
                    if (prefix != null) {
                        PrefixFieldWithParent(parentField, yaml.decodeFromYamlNode(PrefixData.serializer(), prefix))
                    } else if (field != null) {
                        MapFieldWithParent(parentField, field)
                    } else {
                        throw SerializationException("Expected parent field with either prefix or map field: ${node.path.toHumanReadableString()}.")
                    }
                } else {
                    if (prefix != null) {
                        PrefixField(yaml.decodeFromYamlNode(PrefixData.serializer(), prefix))
                    } else if (field != null) {
                        MapField(field)
                    } else if (constant != null) {
                        ConstantField(constant)
                    } else {
                        throw SerializationException("Expected a field ot have a constant, direct map field or prefix field: ${node.path.toHumanReadableString()}.")
                    }
                }

            }

            is YamlNull -> throw SerializationException("Expected field node, found null instead: ${node.path.toHumanReadableString()}.")
            is YamlScalar -> MapField(
                name = node.content
            )

            is YamlTaggedNode -> throw SerializationException("Expected field node, found tagged node instead: ${node.path.toHumanReadableString()}.")
        }
    }

    override fun serialize(encoder: Encoder, value: Field) {
        throw UnsupportedOperationException()
    }
}