/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.PREF_LABEL
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangField
import ch.memobase.schema.mapping.SerializerUtils.missingField
import com.charleskorn.kaml.YamlInput
import com.charleskorn.kaml.yamlMap
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object SubjectSerializer : KSerializer<Subject> {

    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(Subject::class.java.name) {
        element<MultiLangField>(PREF_LABEL)
    }

    override fun deserialize(decoder: Decoder): Subject {
        decoder as YamlInput
        val values = decoder.node.yamlMap
        listOf(
            PREF_LABEL
        ).let { list ->
            values.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found in subject field: ${entry.content}.")
                }
            }
        }
        return Subject(
            prefLabel = extractMultiLangField(values, PREF_LABEL) ?: missingField(PREF_LABEL, Subject::class.java.name),
        )
    }

    override fun serialize(encoder: Encoder, value: Subject) {
        throw UnsupportedOperationException()
    }
}