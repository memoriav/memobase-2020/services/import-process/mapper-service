/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import ch.memobase.helpers.YamlKeys.NAME
import ch.memobase.helpers.YamlKeys.SAME_AS
import ch.memobase.schema.mapping.SerializerUtils.extractField
import ch.memobase.schema.mapping.SerializerUtils.extractMultiLangField
import ch.memobase.schema.mapping.SerializerUtils.missingField
import com.charleskorn.kaml.YamlInput
import com.charleskorn.kaml.yamlMap
import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerializationException
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.descriptors.element
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

object UsageSerializer : KSerializer<Usage> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor(Usage::class.java.name) {
        element<MultiLangField>(NAME)
        element<Field>(SAME_AS)
    }

    override fun deserialize(decoder: Decoder): Usage {
        decoder as YamlInput
        val values = decoder.node.yamlMap
        listOf(
            NAME,
            SAME_AS,
        ).let { list ->
            values.entries.keys.forEach { entry ->
                if (!list.contains(entry.content)) {
                    throw SerializationException("Unknown property found under physical.rights.usage or digital.rights.usage: ${entry.content}.")
                }
            }
        }
        return Usage(
            name = extractMultiLangField(values, NAME) ?: missingField(NAME, Usage::class.java.name),
            sameAs = extractField(values, SAME_AS) ?: missingField(SAME_AS, Usage::class.java.name),
        )
    }

    override fun serialize(encoder: Encoder, value: Usage) {
        throw UnsupportedOperationException()
    }
}