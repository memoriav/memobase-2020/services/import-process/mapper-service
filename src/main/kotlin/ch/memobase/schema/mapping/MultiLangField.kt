/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema.mapping

import kotlinx.serialization.Serializable

@Serializable(with = MultiLangFieldSerializer::class)
sealed class MultiLangField

@Serializable
data class LanguageField(
    val de: Field,
    val fr: Field,
    val it: Field,
): MultiLangField()

@Serializable
data class GermanField(
    val de: Field,
): MultiLangField()

@Serializable
data class FrenchField(
    val fr: Field,
): MultiLangField()

@Serializable
data class ItalianField(
    val it: Field,
): MultiLangField()

@Serializable
data class GermanFrenchField(
    val de: Field,
    val fr: Field,
): MultiLangField()

@Serializable
data class GermanItalianField(
    val de: Field,
    val it: Field,
): MultiLangField()

@Serializable
data class FrenchItalianField(
    val fr: Field,
    val it: Field,
): MultiLangField()






