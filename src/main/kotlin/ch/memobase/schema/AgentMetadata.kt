/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema

import org.apache.jena.rdf.model.Literal

data class AgentMetadata(
    val name: List<Literal>,
    val relationNames: List<List<Literal>>,
    val sameAs: List<String>?,
    val hasVariantNameOfAgent: List<Literal>?,
    val hasBirthDate: String?,
    val hasDeathDate: String?,
    val gender: String?,
    val hasPeriodOfActivity: List<String>?,
    val history: List<Literal>?,
    val isMemberOf: List<Literal>?,
    val descriptiveNote: List<Literal>?,
    val hasProfessionOrOccupation: List<Literal>?,
)
