/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.schema

import ch.memobase.reporting.Report
import ch.memobase.schema.mapping.RecordMap
import kotlinx.serialization.json.JsonObject

data class PipelineData(
    val input: String,
    val config: RecordMap? = null,
    val exception: String,
)


data class PipelineDataParsed(
    val input: JsonObject,
    val config: RecordMap,
    val report: Report?,
)