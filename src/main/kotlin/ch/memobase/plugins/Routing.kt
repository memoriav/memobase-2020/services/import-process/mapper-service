/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase.plugins

import ch.memobase.impl.Parsers
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

fun Application.configureRouting() {
    routing {
        post("/validate") {
            val byteArrayBody = call.receive<ByteArray>()

            val result = Parsers.parseConfigInPipeline(byteArrayBody)
            if (result.first != null) {
                call.respond(HttpStatusCode.OK, Json.encodeToString(buildJsonObject {
                    put("success", true)
                }))
            } else {
                call.respond(HttpStatusCode.OK, Json.encodeToString(buildJsonObject {
                    put("success", false)
                    put("error", result.second)
                }))
            }
        }
    }
}

fun Application.module() {
    configureRouting()
}