/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import org.apache.kafka.common.header.internals.RecordHeader
import org.apache.kafka.common.header.internals.RecordHeaders
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TestOutputTopic
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.TestRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File
import java.nio.charset.Charset

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestPipeline {
    private val resourcePath = "src/test/resources/data"
    private fun readFile(fileName: String, count: Int): String {
        return File("$resourcePath/$count/$fileName").readText(Charset.defaultCharset())
    }

    private fun readFile(fileName: String, directory: String): String {
        return File("$resourcePath/$directory/$fileName").readText(Charset.defaultCharset())
    }

    private fun headers(): RecordHeaders {
        val headers = RecordHeaders()
        headers.add(RecordHeader("sessionId", "test-session-id".toByteArray()))
        headers.add(RecordHeader("recordSetId", "rst-001".toByteArray()))
        headers.add(RecordHeader("institutionId", "ins".toByteArray()))
        return headers
    }

    private fun setupTest(
        count: Int,
        inputKey: String
    ): Triple<TestOutputTopic<String, String>, TestOutputTopic<String, String>, TopologyTestDriver> {
        val settings = App.defineSettings("data/$count/app.yml")
        val testDriver =
            TopologyTestDriver(KafkaTopology(settings).prepare().build(), settings.kafkaStreamsSettings)
        val inputValue = readFile("input.json", count)

        val inputTopic =
            testDriver.createInputTopic(settings.inputTopic, StringSerializer(), StringSerializer())
        val inputRecord = TestRecord(inputKey, inputValue, headers())
        val configTopic = testDriver.createInputTopic(
            settings.appSettings.getProperty("configTopic"), StringSerializer(), StringSerializer()
        )
        // The order is important! Otherwise, a race condition could occur in the tests!!!
        configTopic.pipeInput("rst-001#mapping", readFile("mapping.yml", count))
        inputTopic.pipeInput(inputRecord)
        val outputTopic =
            testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())
        val outputReportTopic = testDriver.createOutputTopic(
            settings.processReportTopic, StringDeserializer(), StringDeserializer()
        )
        return Triple(outputTopic, outputReportTopic, testDriver)
    }

    @Test
    fun `test 1 minimal configuration`() {
        val topics = setupTest(1, "test-1")
        assertAll("test 1 minimal configuration",
            {
                assertThat(topics.first.queueSize)
                    .describedAs("Stream output topic")
                    .isEqualTo(1)
            },
            {
                assertThat(topics.second.queueSize)
                    .describedAs("Report output topic")
                    .isEqualTo(1)
            }
        )
        topics.third.close()
    }

    @Test
    fun `test 2 minimal configuration with digital and physical object`() {
        val topics = setupTest(2, "test-2")
        assertAll("test 2 minimal configuration with digital and physical object",
            {
                assertThat(topics.first.queueSize)
                    .describedAs("Stream output topic")
                    .isEqualTo(1)
            },
            {
                assertThat(topics.second.queueSize)
                    .describedAs("Report output topic")
                    .isEqualTo(1)
            }

        )
        topics.third.close()
    }

    @Test
    fun `test 3 test list input`() {
        val topics = setupTest(3, "test-3")
        assertAll("test 3 test list input",
            {
                assertThat(topics.first.queueSize)
                    .describedAs("Stream output topic")
                    .isEqualTo(1)
            },
            {
                assertThat(topics.second.queueSize)
                    .describedAs("Report output topic")
                    .isEqualTo(1)
            }
        )
        topics.third.close()
    }

    @Test
    fun `test 4 if proxy field in mapping works`() {
        val topics = setupTest(4, "test-4")
        assertAll("test 4 if proxy field in mapping works",
            {
                assertThat(topics.first.queueSize)
                    .describedAs("Stream output topic")
                    .isEqualTo(1)
            },
            {
                assertThat(topics.second.queueSize)
                    .describedAs("Report output topic")
                    .isEqualTo(1)
            }
        )
        topics.third.close()
    }

    @Test
    fun `test 5 rights statement generation`() {
        val topics = setupTest(5, "test-5")
        assertAll("test 5 rights statement generation",
            {
                assertThat(topics.first.queueSize)
                    .describedAs("Stream output topic")
                    .isEqualTo(1)
            },
            {
                assertThat(topics.second.queueSize)
                    .describedAs("Report output topic")
                    .isEqualTo(1)
            }
        )
        topics.third.close()
    }

    @Test
    fun `test 6 generic tests`() {
        val topics = setupTest(6, "test-6")
        assertAll("test 6 generic tests",
            {
                assertThat(topics.first.queueSize)
                    .describedAs("Stream output topic")
                    .isEqualTo(1)
            },
            {
                assertThat(topics.second.queueSize)
                    .describedAs("Report output topic")
                    .isEqualTo(1)
            }
        )
        topics.third.close()
    }

    @Test
    fun `test 7 error message with empty mapping`() {
        val settings = App.defineSettings("data/7/app.yml")
        TopologyTestDriver(KafkaTopology(settings).prepare().build(), settings.kafkaStreamsSettings).use { testDriver ->
            val inputValue = readFile("input.json", "7")
            val inputTopic =
                testDriver.createInputTopic(settings.inputTopic, StringSerializer(), StringSerializer())
            val inputRecord = TestRecord("tst-001-key-text", inputValue, headers())
            val configTopic = testDriver.createInputTopic(
                settings.appSettings.getProperty("configTopic"), StringSerializer(), StringSerializer()
            )
            // The order is important! Otherwise, a race condition could occur in the tests!!!
            configTopic.pipeInput(TestRecord("rst-001#mapping", ""))
            inputTopic.pipeInput(inputRecord)
            val outputTopic =
                testDriver.createOutputTopic(settings.outputTopic, StringDeserializer(), StringDeserializer())
            val outputReportTopic = testDriver.createOutputTopic(
                settings.processReportTopic, StringDeserializer(), StringDeserializer()
            )

            assertAll("test 7 generic tests",
                {
                    assertThat(outputTopic.queueSize)
                        .describedAs("Output stream topic")
                        .isEqualTo(0)
                },
                {
                    assertThat(outputReportTopic.queueSize)
                        .describedAs("Report stream topic")
                        .isEqualTo(1)
                }
            )
        }
    }
}
