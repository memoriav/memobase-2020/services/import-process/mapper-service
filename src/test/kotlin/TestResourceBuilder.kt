package ch.memobase

import ch.memobase.helpers.Reports
import ch.memobase.impl.Parsers
import ch.memobase.impl.ResourceBuilder
import ch.memobase.reporting.ReportStatus
import org.apache.jena.riot.RDFFormat
import org.assertj.core.api.AssertionsForClassTypes.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertAll
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestResourceBuilder {

    private val path = "src/test/resources/data"
    private val cic = "cic"
    private val snp001104782_A01 = "snp-001-104782_A01"
    private val snp001107628_B18 = "snp-001-107628_B18"
    @Test
    fun `test cic example`() {
        val mapping = Parsers.parseConfigInPipeline(File("$path/$cic/mapping.yml").readBytes())
        val source = Parsers.parseInput(File("$path/$cic/input.json").readText())

        val resourceBuilder = ResourceBuilder.initializeBuilder(
            currentKey = "test",
            source = source,
            config = mapping.first!!,
            reports = Reports("step", "version"),
            institutionId = "cic",
            recordSetId = "cic-001",
            isPublished = true
        )

        val result = resourceBuilder.first?.generateRecord()?.value
        val secondResult = result?.resourceBuilder?.generatePhysicalObject(result.resources, result.report)
        val thirdResult = secondResult?.resourceBuilder?.generateDigitalObject(secondResult.resources, secondResult.report)
        val fourthResult = thirdResult?.resourceBuilder?.generateThumbnailObject(thirdResult.resources, thirdResult.report)

        val writtenResult =
            fourthResult?.resourceBuilder?.writeRecord(RDFFormat.TURTLE_LONG, fourthResult.resources, fourthResult.report)

        if (File("$path/output").exists()) {
            writtenResult?.let { File("$path/output/cic-001.ttl").writeText(it.content) }
        }
        assertAll(
            {
                assertThat(writtenResult?.report?.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(writtenResult?.report?.message)
                    .isEqualTo("")
            }
        )
    }

    @Test
    fun `test mfk-005-305560_030 example - fails`() {
        val mapping = Parsers.parseConfigInPipeline(File("$path/mfk-005-305560_030/mapping.yml").readBytes())
        val source = Parsers.parseInput(File("$path/mfk-005-305560_030/input.json").readText())

        val resourceBuilder = ResourceBuilder.initializeBuilder(
            currentKey = "test",
            source = source,
            config = mapping.first!!,
            reports = Reports("step", "version"),
            institutionId = "mfk",
            recordSetId = "mfk-005",
            isPublished = true
        )

        val result = resourceBuilder.first?.generateRecord()?.value
        val secondResult = result?.resourceBuilder?.generatePhysicalObject(result.resources, result.report)
        val thirdResult = secondResult?.resourceBuilder?.generateDigitalObject(secondResult.resources, secondResult.report)
        val fourthResult = thirdResult?.resourceBuilder?.generateThumbnailObject(thirdResult.resources, thirdResult.report)

        val writtenResult =
            fourthResult?.resourceBuilder?.writeRecord(RDFFormat.TTL, fourthResult.resources, fourthResult.report)

        if (File("$path/output").exists()) {
            writtenResult?.let { File("$path/output/mfk-005-305560_030.ttl").writeText(it.content) }
        }
        assertAll(
            {
                assertThat(writtenResult?.report?.status)
                    .isEqualTo(ReportStatus.fatal)
            },
            {
                assertThat(writtenResult?.report?.message)
                    .isEqualTo("PREFIX dc:       <http://purl.org/dc/elements/1.1/>\n" +
                            "PREFIX dcterms:  <http://purl.org/dc/terms/>\n" +
                            "PREFIX ebucore:  <http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#>\n" +
                            "PREFIX edm:      <http://www.europeana.eu/schemas/edm/>\n" +
                            "PREFIX fedora:   <http://fedora.info/definitions/v4/repository#>\n" +
                            "PREFIX foaf:     <http://xmlns.com/foaf/0.1/>\n" +
                            "PREFIX geo:      <http://www.opengis.net/ont/geosparql#>\n" +
                            "PREFIX internal: <https://memobase.ch/internal/>\n" +
                            "PREFIX ldp:      <http://www.w3.org/ns/ldp#>\n" +
                            "PREFIX mbcb:     <https://memobase.ch/institution/>\n" +
                            "PREFIX mbdo:     <https://memobase.ch/digital/>\n" +
                            "PREFIX mbpo:     <https://memobase.ch/physical/>\n" +
                            "PREFIX mbr:      <https://memobase.ch/record/>\n" +
                            "PREFIX mbrs:     <https://memobase.ch/recordSet/>\n" +
                            "PREFIX owl:      <http://www.w3.org/2002/07/owl#>\n" +
                            "PREFIX rdau:     <http://rdaregistry.info/Elements/u/>\n" +
                            "PREFIX rdf:      <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                            "PREFIX rdfs:     <http://www.w3.org/2000/01/rdf-schema#>\n" +
                            "PREFIX rico:     <https://www.ica.org/standards/RiC/ontology#>\n" +
                            "PREFIX schema:   <http://schema.org/>\n" +
                            "PREFIX sh:       <http://www.w3.org/ns/shacl#>\n" +
                            "PREFIX skos:     <http://www.w3.org/2004/02/skos/core#>\n" +
                            "PREFIX test:     <https://test.memobase.ch/>\n" +
                            "PREFIX wd:       <http://www.wikidata.org/entity/>\n" +
                            "PREFIX wdt:      <http://www.wikidata.org/prop/direct/>\n" +
                            "PREFIX wdtn:     <http://www.wikidata.org/prop/direct-normalized/>\n" +
                            "PREFIX xsd:      <http://www.w3.org/2001/XMLSchema#>\n" +
                            "\n" +
                            "[ rdf:type     sh:ValidationReport;\n" +
                            "  sh:conforms  false;\n" +
                            "  sh:result    [ rdf:type                      sh:ValidationResult;\n" +
                            "                 sh:focusNode                  mbdo:mfk-005-305560_030-1;\n" +
                            "                 sh:resultMessage              \"minCount[2]: Invalid cardinality: expected min 2: Got count = 1\";\n" +
                            "                 sh:resultPath                 rico:isOrWasRegulatedBy;\n" +
                            "                 sh:resultSeverity             sh:Violation;\n" +
                            "                 sh:sourceConstraintComponent  sh:MinCountConstraintComponent;\n" +
                            "                 sh:sourceShape                [] \n" +
                            "               ]\n" +
                            "] .\n" +
                            "")
            }
        )
    }

    @Test
    fun `test snp-001-104782_A01 example`() {
        val mapping = Parsers.parseConfigInPipeline(File("$path/$snp001104782_A01/mapping.yml").readBytes())
        val source = Parsers.parseInput(File("$path/$snp001104782_A01/input.json").readText())

        val resourceBuilder = ResourceBuilder.initializeBuilder(
            currentKey = "test",
            source = source,
            config = mapping.first!!,
            reports = Reports("step", "version"),
            institutionId = "cic",
            recordSetId = "cic-001",
            isPublished = true
        )

        val result = resourceBuilder.first?.generateRecord()?.value
        val secondResult = result?.resourceBuilder?.generatePhysicalObject(result.resources, result.report)
        val thirdResult = secondResult?.resourceBuilder?.generateDigitalObject(secondResult.resources, secondResult.report)
        val fourthResult = thirdResult?.resourceBuilder?.generateThumbnailObject(thirdResult.resources, thirdResult.report)

        val writtenResult =
            fourthResult?.resourceBuilder?.writeRecord(RDFFormat.TTL, fourthResult.resources, fourthResult.report)

        if (File("$path/output").exists()) {
            writtenResult?.let { File("$path/output/$snp001104782_A01.ttl").writeText(it.content) }
        }
        assertAll(
            {
                assertThat(writtenResult?.report?.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(writtenResult?.report?.message)
                    .isEqualTo("")
            }
        )
    }
    @Test
    fun `test snp-001-107628_B18 example`() {
        val mapping = Parsers.parseConfigInPipeline(File("$path/$snp001107628_B18/mapping.yml").readBytes())
        val source = Parsers.parseInput(File("$path/$snp001107628_B18/input.json").readText())

        val resourceBuilder = ResourceBuilder.initializeBuilder(
            currentKey = "test",
            source = source,
            config = mapping.first!!,
            reports = Reports("step", "version"),
            institutionId = "cic",
            recordSetId = "cic-001",
            isPublished = true
        )

        val result = resourceBuilder.first?.generateRecord()?.value
        val secondResult = result?.resourceBuilder?.generatePhysicalObject(result.resources, result.report)
        val thirdResult = secondResult?.resourceBuilder?.generateDigitalObject(secondResult.resources, secondResult.report)
        val fourthResult = thirdResult?.resourceBuilder?.generateThumbnailObject(thirdResult.resources, thirdResult.report)

        val writtenResult =
            fourthResult?.resourceBuilder?.writeRecord(RDFFormat.TTL, fourthResult.resources, fourthResult.report)

        if (File("$path/output").exists()) {
            writtenResult?.let { File("$path/output/$snp001107628_B18.ttl").writeText(it.content) }
        }

        assertAll(
            {
                assertThat(writtenResult?.report?.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(writtenResult?.report?.message)
                    .isEqualTo("")
            }
        )
    }


    @Test
    fun `test person const name`() {
        val mapping = Parsers.parseConfigInPipeline(File("$path/test-person-const-name/mapping.yml").readBytes())
        val source = Parsers.parseInput(File("$path/test-person-const-name/input.json").readText())

        val resourceBuilder = ResourceBuilder.initializeBuilder(
            currentKey = "test",
            source = source,
            config = mapping.first!!,
            reports = Reports("step", "version"),
            institutionId = "cic",
            recordSetId = "cic-001",
            isPublished = true
        )

        val result = resourceBuilder.first?.generateRecord()?.value
        val secondResult = result?.resourceBuilder?.generatePhysicalObject(result.resources, result.report)
        val thirdResult = secondResult?.resourceBuilder?.generateDigitalObject(secondResult.resources, secondResult.report)
        val fourthResult = thirdResult?.resourceBuilder?.generateThumbnailObject(thirdResult.resources, thirdResult.report)

        val writtenResult =
            fourthResult?.resourceBuilder?.writeRecord(RDFFormat.TTL, fourthResult.resources, fourthResult.report)

        if (File("$path/output").exists()) {
            writtenResult?.let { File("$path/output/test-person-const-name.ttl").writeText(it.content) }
        }

        assertAll(
            {
                assertThat(writtenResult?.report?.status)
                    .isEqualTo(ReportStatus.success)
            },
            {
                assertThat(writtenResult?.report?.message)
                    .isEqualTo("")
            }
        )
    }
}