/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.builder.DigitalObject
import ch.memobase.builder.Record
import ch.memobase.builder.Thumbnail
import ch.memobase.impl.Validate
import ch.memobase.rdf.EBUCORE
import org.apache.jena.riot.RDFDataMgr
import org.apache.jena.riot.RDFFormat
import org.assertj.core.api.AssertionsForClassTypes.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestShapeValidation {

    @BeforeAll
    fun printShapeModel() {
        if (File("src/test/resources/data/shape").exists()) {
            Validate.printShapeModel("src/test/resources/data/shape", "output")
        }
    }

    @Test
    fun `test minimal record validation`() {
        val record = Record(
            sourceId = "sourceId",
            type = "Film",
            recordSetId = "recordSetId",
            institutionId = "institutionId",
            hasSponsoringAgent = true,
            isPublished = true,
        )
        record.addMainTitle(
            listOf(
                record.literal("A title")
            )
        )
        record.addOriginalIdentifier(record.literal("sourceId"))
        val result = Validate.validateRecordModel(record.model)
        if (File("src/test/resources/data/shape").exists()) {
            RDFDataMgr.write(
                File("src/test/resources/data/output", "record-no-lang-title").outputStream(),
                record.model,
                RDFFormat.TURTLE_PRETTY
            )
        }

        assertThat(result)
            .describedAs("Validation error found")
            .isEqualTo("")
    }

    @Test
    fun `test full record validation`() {
        val record = Record(
            sourceId = "sourceId",
            type = "Film",
            recordSetId = "recordSetId",
            institutionId = "institutionId",
            hasSponsoringAgent = true,
            isPublished = true,
        )
        record.addMainTitle(
            listOf(
                record.langLiteral("Main Title De", "de"),
                record.langLiteral("Main Title Fr", "fr"),
                record.langLiteral("Main Title It", "it"),
            )
        )
        record.addOriginalIdentifier(record.literal("sourceId"))
        record.addOldMemobaseIdentifier(record.literal("oldMemobaseId"))

        record.addSeriesTitle(
            listOf(record.literal("A Series Title"))
        )

        record.addBroadcastTitle(
            listOf(
                record.langLiteral("Broadcast Title De", "de"),
                record.langLiteral("Broadcast Title Fr", "fr"),
                record.langLiteral("Broadcast Title It", "it")
            )
        )

        if (File("src/test/resources/data/shape").exists()) {
            RDFDataMgr.write(
                File("src/test/resources/data/output", "record-lang-title").outputStream(),
                record.model,
                RDFFormat.TURTLE_PRETTY
            )
        }

        val result = Validate.validateRecordModel(record.model)
        assertThat(result)
            .describedAs("Validation error found")
            .isEqualTo("")
    }

    @Test
    fun `test thumbnail shape validation`() {
        val thumbnail = Thumbnail(
            originalId = "originalId",
            recordSetId = "recordSetId",
            institutionId = "institutionId",
            count = 1
        )
        thumbnail.addLiteral(EBUCORE.locator, thumbnail.literal("https://thumbnail.com/file"))
        thumbnail.addRecord(
            Record(
                sourceId = "sourceId",
                type = "Film",
                recordSetId = "recordSetId",
                institutionId = "institutionId",
                hasSponsoringAgent = true,
                isPublished = true,
            )
        )
        thumbnail.addIsDerivedFromInstantiation(
            DigitalObject(
                sourceId = "sourceId",
                recordSetId = "recordSetId",
                institutionId = "institutionId",
                count = 1,
                hasProxyType = false
            )
        )
        val result = Validate.validateThumbnailShape(thumbnail.model)
        assertThat(result)
            .describedAs("Validation error found")
            .isEqualTo("")
    }
}