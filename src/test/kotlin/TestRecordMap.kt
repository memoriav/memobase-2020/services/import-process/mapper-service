/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.schema.mapping.RecordMap
import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.decodeFromStream
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.io.path.isDirectory
import kotlin.io.path.name


/**
 * Tests with the local environment to ensure that all current mappings can be parsed and processed.
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestRecordMap {

    private val mappingPath = System.getenv("TEST_MAPPING_PATH") ?: "import-process/recordsets"

    @Test
    fun `test record map with prod record set mappings`() {
        var countExceptions = 0
        Files.walk(Paths.get(mappingPath, "prod"))
            .filter { path -> !path.isDirectory() }
            .filter { path -> path.name.endsWith("mapping.yml") }
            .map { path -> path.toFile() }
            .map { file -> Pair(file.inputStream(), file.parent) }
            .map { inputStream ->
                try {
                    Pair(Yaml.default.decodeFromStream<RecordMap>(inputStream.first), inputStream.second)
                } catch (ex: Exception) {
                    Pair(ex, inputStream.second)
                }
            }.forEach { result ->
                if (result.first !is RecordMap) {
                    countExceptions += 1
                    println(result.second)
                    println(result.first)
                }
            }
        println("Exceptions: $countExceptions")
        assert(countExceptions == 0)
    }

    @Test
    fun `test record map with stage record set mappings`() {
        var countExceptions = 0
        Files.walk(Paths.get(mappingPath, "stage"))
            .filter { path -> !path.isDirectory() }
            .filter { path -> path.name.endsWith("mapping.yml") }
            .map { path -> path.toFile() }
            .map { file -> Pair(file.inputStream(), file.parent) }
            .map { inputStream ->
                try {
                    Pair(Yaml.default.decodeFromStream<RecordMap>(inputStream.first), inputStream.second)
                } catch (ex: Exception) {
                    Pair(ex, inputStream.second)
                }
            }.forEach { result ->
                if (result.first !is RecordMap) {
                    countExceptions += 1
                    println(result.second)
                    println(result.first)
                }
            }
        println("Exceptions: $countExceptions")
        assert(countExceptions == 0)
    }

    @Test
    fun `test record map with test record set mappings`() {
        var countExceptions = 0
        Files.walk(Paths.get(mappingPath, "test"))
            .filter { path -> !path.isDirectory() }
            .filter { path -> path.name.endsWith("mapping.yml") }
            .map { path -> path.toFile() }
            .map { file -> Pair(file.inputStream(), file.parent) }
            .map { inputStream ->
                try {
                    Pair(Yaml.default.decodeFromStream<RecordMap>(inputStream.first), inputStream.second)
                } catch (ex: Exception) {
                    Pair(ex, inputStream.second)
                }
            }.forEach { result ->
                if (result.first !is RecordMap) {
                    countExceptions += 1
                    println(result.second)
                    println(result.first)
                }
            }
        println("Exceptions: $countExceptions")
        assert(countExceptions == 0)
    }

    @Test
    fun `test specific mapping`() {
        var countExceptions = 0
        Files.walk(Paths.get(mappingPath, "test"))
            .filter { path -> !path.isDirectory() }
            .filter { path -> path.name.endsWith("mapping.yml") }
            .map { path -> path.toFile() }
            .map { file -> Pair(file.inputStream(), file.parent) }
            .filter { file -> file.second.contains("ula-001") }
            .map { inputStream ->
                try {
                    Pair(Yaml.default.decodeFromStream<RecordMap>(inputStream.first), inputStream.second)
                } catch (ex: Exception) {
                    Pair(ex, inputStream.second)
                }
            }.forEach { result ->
                if (result.first !is RecordMap) {
                    countExceptions += 1
                    println(result.second)
                    println(result.first)
                }
            }
        println("Exceptions: $countExceptions")
        assert(countExceptions == 0)
    }
}