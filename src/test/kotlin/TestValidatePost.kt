/*
 * Copyright (C) 2020 - present Jonas Waeber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package ch.memobase

import ch.memobase.impl.Server
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonPrimitive
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestValidatePost {
    private val client = HttpClient(CIO)

    @BeforeAll
    fun init() {
        Server.startServer()
    }

    @Test
    fun testValidatePost() {
        runBlocking {
            val response = client.post {
                url {
                    protocol = URLProtocol.HTTP
                    port = 8080
                    host = "0.0.0.0"
                    path("/validate")
                }
                setBody(File("src/test/resources/data/mapping.yml").readBytes())
            }

            val result: JsonObject = Json.decodeFromString(response.bodyAsText(Charsets.UTF_8))
            println(result)
            assert(result["success"]?.jsonPrimitive?.content?.toBooleanStrict() ?: false)
        }
    }
}