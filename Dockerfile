FROM gradle:8.8-jdk21-jammy AS build
ADD . .
RUN gradle --no-daemon --no-scan --no-build-cache distTar && \
  mkdir /dist && \
  tar xf $(find ./build/distributions/ -name *.tar | head -1) && \
  mv app /dist

FROM eclipse-temurin:21-jre-jammy
COPY --from=build /dist/ /
CMD [ "/app/bin/mapper-service" ]